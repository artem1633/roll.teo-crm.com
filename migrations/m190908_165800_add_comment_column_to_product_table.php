<?php

use yii\db\Migration;

/**
 * Handles adding comment to table `product`.
 */
class m190908_165800_add_comment_column_to_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('product', 'comment', $this->text()->after('additional_information')->comment('Примечание'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product', 'comment');
    }
}
