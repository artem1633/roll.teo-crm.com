<?php

use yii\db\Migration;

/**
 * Class m200413_065500_add_columns_to_table_user
 */
class m200413_065500_add_columns_to_table_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
		$this->addColumn('user', 'discount_came', $this->string(10)->after('name')->comment('Скидка для Came'));
		$this->addColumn('user', 'discount_aluteh', $this->string(10)->after('discount_came')->comment('Скидка для Алютех'));
		$this->addColumn('user', 'discount_reika', $this->string(10)->after('discount_aluteh')->comment('Скидка для Рейки'));
		$this->addColumn('user', 'discount_other', $this->string(10)->after('discount_reika')->comment('Скидка для Прочего'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        echo "m200413_065500_add_columns_to_table_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200413_065500_add_columns_to_table_user cannot be reverted.\n";

        return false;
    }
    */
}
