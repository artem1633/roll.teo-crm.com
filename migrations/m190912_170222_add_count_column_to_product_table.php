<?php

use yii\db\Migration;

/**
 * Handles adding count to table `product`.
 */
class m190912_170222_add_count_column_to_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('product', 'count', $this->integer()->unsigned()->after('price')->comment('Кол-во'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product', 'count');
    }
}
