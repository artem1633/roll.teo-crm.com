<?php

use yii\db\Migration;

/**
 * Class m200227_133503_add_columns_to_product_table
 */
class m200227_133503_add_columns_to_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
		$this->addColumn('product', 'tail_pr', $this->integer()->unsigned()->after('dyeing')->comment('Хвост прямой'));
		$this->addColumn('product', 'dop_pult', $this->integer()->unsigned()->after('tail_pr')->comment('Доп. пульт'));
		$this->addColumn('product', 'compl_proflist', $this->integer()->unsigned()->after('dop_pult')->comment('Заполнение профлистом'));
		$this->addColumn('product', 'compl_proflist_ko', $this->integer()->unsigned()->after('compl_proflist')->comment('Заполнение профлистом калитка отдельностоящая'));
		$this->addColumn('product', 'toothed_rack_count', $this->integer()->unsigned()->after('compl_proflist_ko')->comment('Количество реек зубчатых'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('product', 'tail_pr');
		$this->dropColumn('product', 'dop_pult');
		$this->dropColumn('product', 'compl_proflist');
		$this->dropColumn('product', 'compl_proflist_ko');
    }
}
