<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company`.
 */
class m191126_144450_create_company_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('company', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'email' => $this->string()->comment('Почта'),
            'phone' => $this->string()->comment('Телефон'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('company');
    }
}
