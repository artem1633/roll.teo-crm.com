<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m190907_152811_create_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Менеджер'),
            'name' => $this->string()->comment('Наименование'),
            'address' => $this->string()->comment('Адрес'),
            'client' => $this->string()->comment('Клиент'),
            'date' => $this->date()->comment('Дата'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-order-user_id',
            'order',
            'user_id'
        );

        $this->addForeignKey(
            'fk-order-user_id',
            'order',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-order-user_id',
            'order'
        );

        $this->dropIndex(
            'idx-order-user_id',
            'order'
        );

        $this->dropTable('order');
    }
}
