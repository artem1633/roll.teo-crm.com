<?php

use yii\db\Migration;

/**
 * Handles adding name to table `product`.
 */
class m190910_182346_add_name_column_to_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('product', 'name', $this->string()->after('id')->comment('Название'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product', 'name');
    }
}
