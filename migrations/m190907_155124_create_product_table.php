<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m190907_155124_create_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заказ'),
            'user_id' => $this->integer()->comment('Менеджер'),
            'price' => $this->float()->comment('Цена'),
            'category' => $this->integer()->comment('Категория'),
            'height' => $this->float()->comment('Высота'),
            'width' => $this->float()->comment('Ширина'),
			'quantity' => $this->integer()->comment('Кол-во'),
			'rollback_side' => $this->integer()->comment('Сторона отката(вид изнутри)'),
			'staff_with_professional_sheet' => $this->integer()->comment('Укомплектовать профлистом'),
			'profiled_sheet_S8045mm' => $this->integer()->comment('Профлист С8 0,45мм цвет'),
			'filling_out' => $this->integer()->comment('Заполнение'),
			'beam' => $this->integer()->comment('Балка'),
			'builtin_gate' => $this->integer()->comment('Калитка встроеная'),
			'wicket_width' => $this->integer()->comment('Ширина калитки встроенной, мм'),
			'gate_location_w' => $this->integer()->comment('Расположение калитки встроенной'),
			'gate_opening_w' => $this->integer()->comment('Открывание калитки встроенной'),
			'wicket_lock' => $this->integer()->comment('Замок калитки встроенной'),
			'tail_distance' => $this->integer()->comment('расстояние от хвоста, мм'),
			'filling_out_freestanding_gate' => $this->integer()->comment('Заполнение(калитка отдельностоящая)'),
			'staff_with_professional_sheet_ko' => $this->integer()->comment('Укомплектовать профлистом(калитка отдельностоящая)'),
			'profiled_sheet_S8045mm_ko' => $this->integer()->comment('Профлист С8 0,45мм цвет(калитка отдельностоящая)'),
			'type_freestanding_gate' => $this->integer()->comment('Тип(калитка отдельностоящая)'),
			'frame_width' => $this->integer()->comment('Ширина рамы'),
			'frame_height' => $this->integer()->comment('Высота от верха рамы до низа калитки'),
			'frame_profile' => $this->integer()->comment('Профиль рамы'),
			'gate_columns' => $this->integer()->comment('Ширина по столбам'),
			'gate_height' => $this->integer()->comment('Высота калитки'),
			'columns_r' => $this->integer()->comment('Столбы'),
			'height60x60' => $this->integer()->comment('Высота(60х60)'),
			'height80x80' => $this->integer()->comment('Высота(80х80)'),
			'height_without_cf' => $this->integer()->comment('Высота(без столбов и рамы), мм'),
			'width_without_cf' => $this->integer()->comment('Ширина(без столбов и рамы), мм'),
			'gate_opening' => $this->integer()->comment('Открывание калитки'),
			'hinges' => $this->integer()->comment('Петли'),
			'opening_side' => $this->integer()->comment('Сторона открывания'),
			'lock_gate' => $this->integer()->comment('Замок для калитки'),
			'toothed_rack' => $this->integer()->comment('Рейка зубчатая'),
			'tail' => $this->integer()->comment('Хвост'),
			'locking_device' => $this->integer()->comment('Запорное устройство'),
			'dyeing' => $this->integer()->comment('Покраска'),
			'channel_P12' => $this->float()->comment('Швеллер П12'),
			'concrete_mortgage' => $this->integer()->comment('Закладная для бетонирования'),
			'pile_57x2000mm' => $this->integer()->comment('Свая 57х2000мм'),
			'heading_57' => $this->integer()->comment('Оголовок 57'),
			'pile_76x2000mm' => $this->integer()->comment('Свая 76х2000мм'),
			'heading_76' => $this->integer()->comment('Оголовок 76'),
			'pile_89x2000mm' => $this->integer()->comment('Свая 89х2000мм'),
			'heading_89' => $this->integer()->comment('Оголовок 89'),
			'pile_108x2000mm' => $this->integer()->comment('Свая 108х2000мм'),
			'heading_108' => $this->integer()->comment('Оголовок 108'),
			'automation' => $this->integer()->comment('Автоматика'),
			'drive_unit' => $this->integer()->comment('Привод'),
			'photocells' => $this->integer()->comment('Фотоэлементы'),
			'lamp' => $this->integer()->comment('Лампа'),
			'combo_kit' => $this->integer()->comment('Комплект Combo'),
			'antenna' => $this->integer()->comment('Антенна'),
            'delivery' => $this->string()->comment('Доставка'),
            'install' => $this->string()->comment('Монтаж/Демонтаж'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-product-order_id',
            'product',
            'order_id'
        );

        $this->addForeignKey(
            'fk-product-order_id',
            'product',
            'order_id',
            'order',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-product-user_id',
            'product',
            'user_id'
        );

        $this->addForeignKey(
            'fk-product-user_id',
            'product',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-product-user_id',
            'product'
        );

        $this->dropIndex(
            'idx-product-user_id',
            'product'
        );

        $this->dropForeignKey(
            'fk-product-order_id',
            'product'
        );

        $this->dropIndex(
            'idx-product-order_id',
            'product'
        );

        $this->dropTable('product');
    }
}
