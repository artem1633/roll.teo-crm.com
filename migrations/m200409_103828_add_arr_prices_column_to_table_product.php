<?php

use yii\db\Migration;

/**
 * Class m200409_103828_add_arr_price_column_to_table_product
 */
class m200409_103828_add_arr_prices_column_to_table_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
		$this->addColumn('product', 'arr_prices', $this->string(5000)->after('price')->comment('JSON с ценами по элементам заказа'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        echo "m200409_103828_add_arr_price_column_to_table_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200409_103828_add_arr_price_column_to_table_product cannot be reverted.\n";

        return false;
    }
    */
}
