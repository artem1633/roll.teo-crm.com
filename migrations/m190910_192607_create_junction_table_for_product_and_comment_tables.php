<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_comment`.
 * Has foreign keys to the tables:
 *
 * - `product`
 * - `comment`
 */
class m190910_192607_create_junction_table_for_product_and_comment_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product_comment', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'comment_id' => $this->integer(),
        ]);

        // creates index for column `product_id`
        $this->createIndex(
            'idx-product_comment-product_id',
            'product_comment',
            'product_id'
        );

        // add foreign key for table `product`
        $this->addForeignKey(
            'fk-product_comment-product_id',
            'product_comment',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );

        // creates index for column `comment_id`
        $this->createIndex(
            'idx-product_comment-comment_id',
            'product_comment',
            'comment_id'
        );

        // add foreign key for table `comment`
        $this->addForeignKey(
            'fk-product_comment-comment_id',
            'product_comment',
            'comment_id',
            'comment',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `product`
        $this->dropForeignKey(
            'fk-product_comment-product_id',
            'product_comment'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            'idx-product_comment-product_id',
            'product_comment'
        );

        // drops foreign key for table `comment`
        $this->dropForeignKey(
            'fk-product_comment-comment_id',
            'product_comment'
        );

        // drops index for column `comment_id`
        $this->dropIndex(
            'idx-product_comment-comment_id',
            'product_comment'
        );

        $this->dropTable('product_comment');
    }
}
