<?php

use yii\db\Migration;

/**
 * Handles adding color to table `product`.
 */
class m200227_065708_add_color_column_to_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
		$this->addColumn('product', 'color', $this->integer()->unsigned()->after('dyeing')->comment('Цвет'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
		$this->dropColumn('product', 'color');
    }
}
