<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company_address`.
 */
class m191126_144550_create_company_address_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('company_address', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->comment('Компания'),
            'address' => $this->string(500)->comment('Адрес'),
        ]);

        $this->createIndex(
            'idx-company_address-company_id',
            'company_address',
            'company_id'
        );

        $this->addForeignKey(
            'fk-company_address-company_id',
            'company_address',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-company_address-company_id',
            'company_address'
        );

        $this->dropIndex(
            'idx-company_address-company_id',
            'company_address'
        );

        $this->dropTable('company_address');
    }
}
