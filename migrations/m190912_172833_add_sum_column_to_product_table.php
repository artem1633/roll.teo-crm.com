<?php

use yii\db\Migration;

/**
 * Handles adding sum to table `product`.
 */
class m190912_172833_add_sum_column_to_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('product', 'sum', $this->float()->after('price')->comment('Сумма'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product', 'sum');
    }
}