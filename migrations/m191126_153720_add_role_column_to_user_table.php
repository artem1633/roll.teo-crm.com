<?php

use yii\db\Migration;

/**
 * Handles adding role to table `user`.
 */
class m191126_153720_add_role_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'role', $this->integer()->after('login')->comment('Роль'));

        \app\models\User::updateAll(['role' => \app\models\User::ROLE_ADMIN]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'role');
    }
}
