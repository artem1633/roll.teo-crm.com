<?php

use yii\db\Migration;

/**
 * Handles dropping parent_param_id from table `param`.
 */
class m190908_170258_drop_parent_param_id_column_from_param_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropForeignKey(
            'fk-param-parent_param_id',
            'param'
        );

        $this->dropIndex(
            'idx-param-parent_param_id',
            'param'
        );

        $this->dropColumn('param', 'parent_param_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('param', 'parent_param_id', $this->integer()->after('name')->comment('Родительский параметр'));

        $this->createIndex(
            'idx-param-parent_param_id',
            'param',
            'parent_param_id'
        );

        $this->addForeignKey(
            'fk-param-parent_param_id',
            'param',
            'parent_param_id',
            'param',
            'id',
            'SET NULL'
        );
    }
}
