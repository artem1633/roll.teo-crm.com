<?php

namespace app\controllers;

use Yii;
use yii\web\UploadedFile;
use app\models\Image;
use yii\web\Controller;

class AdminController extends Controller
{
	public function actionUpdateImg()
    {
		$model = new Image;
        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');
			$url = $model->url;
			if(!empty($model->image)) {
            	$model->upload($url);
				
			} 
			Yii::$app->session->setFlash('success', 'Чертеж обновлен!');
			
        }
		
        return $this->render('/admin/update-img', [
            'model' => $model,
        ]);
	}
}