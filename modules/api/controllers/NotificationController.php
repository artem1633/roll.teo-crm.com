<?php

namespace app\modules\api\controllers;

use app\models\AddressSpecification;
use app\models\User;
use yii\helpers\VarDumper;
use yii\rest\ActiveController;
use yii\web\Response;

/**
 * Default controller for the `api` module
 */
class NotificationController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionSendReloadMessage()
    {
        $date = date('Y-m-d');

        $specifications = AddressSpecification::find()->where(['reload_date' => $date])->all();

        VarDumper::dump($specifications, 10, true);

        foreach ($specifications as $specification)
        {
            $users = User::find()->where(['role' => User::ROLE_PRODUCTION])->all();

            foreach ($users as $user){
                $user->sendEmailMessage("Наступила дата отгрузки", "Наступила дата отгрузки по спецификации {$specification->specification}");
            }
        }
    }
}
