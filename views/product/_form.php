<?php
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use app\models\Param;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */

/**
 * @param int $type
 * @return array
 */
function getParamsByType($type){
    return \yii\helpers\ArrayHelper::map(Param::find()->where(['type' => $type])->all(), 'id', 'name');
}

$formatJs = <<< 'JS'
var formatRepo = function (color) {
    if (color.loading) {
        return color.text;
    }
    var markup = '<div class="row">' + 
    '<div class="col-sm-12">' +
        '<b style="margin-left:5px">' + color.code + '</b>' + 
        '<div style="display: inline-block; height: 10px; width: 30px; margin-left: 5px; background-color: '+color.rgb_hex+';"></div>'+
     '</div>' +
'</div>';
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (color) {
    return color.text || color.code;
}
JS;

// Register the formatting script
$this->registerJs($formatJs, View::POS_HEAD);

// script to parse the results into the format expected by Select2
$resultsJs = <<< JS
function (data, params) {
    params.page = params.page || 1;
    return {
        results: data.items,
        pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
}
JS;

/*if($model->isNewRecord == false){
    $model->comments = \yii\helpers\ArrayHelper::getColumn(\app\models\ProductComment::find()->where(['product_id' => $model->id])->all(), 'comment_id');
} else {
    $model->count = 1;
    $model->price = 0;
}*/

?>

<script type="text/javascript"> 

$(function(){
	$('.field-product-profiled_sheet_s8045mm').hide();
	$('#product-filling_out').change(function() {
		if($(this).val()!=3) {
			$('.field-product-staff_with_professional_sheet').hide();
			$('.field-product-profiled_sheet_s8045mm').hide();
			$('.field-product-compl_proflist').hide();
			$(this).parent().parent().siblings().children().children().val(0);
		} else {
			$('.field-product-staff_with_professional_sheet').show();
		}
	});
	$('.field-product-compl_proflist').hide();	
	$('#product-profiled_sheet_s8045mm').change(function() {
		if($(this).val()==1) {
			$('.field-product-compl_proflist').show();
		} else {
			$('.field-product-compl_proflist').hide();	
		}
	});
	$('#product-staff_with_professional_sheet').change(function() {
		if($(this).val()==0) {
			$('#product-profiled_sheet_s8045mm').val(0);	
		}
		if($(this).val()==1) {
			$('.field-product-profiled_sheet_s8045mm').hide();
			$('#product-profiled_sheet_s8045mm').val(0);
		}
		if($(this).val()==2) {
			$('.field-product-profiled_sheet_s8045mm').show();	
		}
	});
	$('.field-product-tail_pr').hide();
	$('#product-tail').change(function() {
		if($(this).val()!=2) {
			$('#product-tail_pr').val(0);
			$(this).parent().parent().siblings().children().children().val(0);
		} else {
			$('#product-tail_pr').val(2);
		}
	});
	$('.row-b-gate').children().children().hide();
	$('.row-b-gate .field-product-builtin_gate').show();
	$('#product-builtin_gate').change(function() {
		if($(this).val()!=2) {
			$('.row-b-gate').children().children().hide();
			$('.row-b-gate .field-product-builtin_gate').show();
			$('.field-product-tail_distance').hide();
			$(this).parent().parent().siblings().children().children().val(0);
		} else {
			$('.row-b-gate').children().children().show();
			$('.field-product-tail_distance').hide();
		}
	});
	$('.field-product-toothed_rack_count').hide();
	$('#product-toothed_rack').change(function() {
		if($(this).val()==0) {
			$('.field-product-toothed_rack_count').hide();
			$(this).parent().parent().siblings().children().children().val();
		} else {
			$('.field-product-toothed_rack_count').show();
		}
	});
	$('.row-out-gate').children().children().hide();
	$('.row-out-gate .field-product-type_freestanding_gate').show();
	$('#product-type_freestanding_gate').change(function() {
		if($(this).val()==0) {
			$('.row-out-gate').children().children().hide();
			$('.row-out-gate .field-product-type_freestanding_gate').show();
			$(this).parent().parent().siblings().children().children().val(0);
			$('#product-filling_out_freestanding_gate').val(3);
			//$('.field-product-profiled_sheet_s8045mm_ko').hide();
		} else {
			$('.row-out-gate').children().children().show();
		}
		if($('#product-filling_out_freestanding_gate').val()!=3) {
			$('.field-product-staff_with_professional_sheet_ko').hide();
			$('#product-staff_with_professional_sheet_ko').val(0);
		} else {
			if($(this).val()!=0) {
				$('.field-product-staff_with_professional_sheet_ko').show();
			}
		}
		if($('#product-profiled_sheet_s8045mm_ko').val()!=1) {
			$('.field-product-compl_proflist_ko').hide();
			$('#product-compl_proflist_ko').val(0);
		} else {
			if($(this).val()!=0) {
				$('.field-product-compl_proflist_ko').show();
			}
		}
		$('.field-product-profiled_sheet_s8045mm_ko').hide();
		$('.field-product-compl_proflist_ko').hide();
		$('#product-staff_with_professional_sheet_ko').val(0);
		$('#product-compl_proflist_ko').val(0);
	});
	/*$('#product-gate_location_w').change(function() {
		if($(this).val()!=2) {
			$('.field-product-tail_distance').hide();
			$('#product-tail_distance').val(0);
		} else {
			$('.field-product-tail_distance').show();
		}
	});*/
	$('.field-product-profiled_sheet_s8045mm_ko').hide();
	$('#product-filling_out_freestanding_gate').change(function() {
		if($(this).val()!=3) {
			$('.field-product-profiled_sheet_s8045mm_ko').hide();
			$('.field-product-staff_with_professional_sheet_ko').hide();
			$('.field-product-compl_proflist_ko').hide();
			$('#product-profiled_sheet_s8045mm_ko').val(0);
			$('#product-compl_proflist_ko').val(0);
		} else {
			$('.field-product-staff_with_professional_sheet_ko').show();
			$('#product-staff_with_professional_sheet_ko').val(0);
			
		}
	});
	$('.field-product-profiled_sheet_s8045mm_ko').hide();
	$('#product-staff_with_professional_sheet_ko').change(function() {
		if($(this).val()==0) {
			$('#product-profiled_sheet_s8045mm_ko').val(0);	
		}
		if($(this).val()==1) {
			$('.field-product-profiled_sheet_s8045mm_ko').hide();
			$('.field-product-profiled_sheet_s8045mm_ko').hide();
			$('#product-profiled_sheet_s8045mm_ko').val(0);
		}
		if($(this).val()==2) {
			$('.field-product-profiled_sheet_s8045mm_ko').show();	
		}
	});
	$('.field-product-compl_proflist_ko').hide();	
	$('#product-profiled_sheet_s8045mm_ko').change(function() {
		if($(this).val()==1) {
			$('.field-product-compl_proflist_ko').show();
		} else {
			$('.field-product-compl_proflist_ko').hide();	
		}
	});
	$('.in-frame-choose').hide();
	$('.on-column-choose').hide();
	$('.out-column-frame-choose').hide();
	$('#product-type_freestanding_gate').change(function() {
		if($(this).val()==1) {
			$('.in-frame-choose').show();
			$('.on-column-choose').hide();
			$('.out-column-frame-choose').hide();
			$('.on-column-choose').children().children().children().children().val(0);
			$('.out-column-frame-choose').children().children().children().children().val(0);
		}
		if($(this).val()==2) {
			$('.in-frame-choose').hide();
			$('.on-column-choose').show();
			$('.out-column-frame-choose').hide();
			$('.in-frame-choose').children().children().children().children().val(0);
			$('.out-column-frame-choose').children().children().children().children().val(0);
		}
		if($(this).val()==3) {
			$('.in-frame-choose').hide();
			$('.on-column-choose').hide();
			$('.out-column-frame-choose').show();
			$('.on-column-choose').children().children().children().children().val(0);
			$('.in-frame-choose').children().children().children().children().val(0);
		}
		if($(this).val()==0) {
			$('.in-frame-choose').hide();
			$('.on-column-choose').hide();
			$('.out-column-frame-choose').hide();
			$('.in-frame-choose').children().children().children().children().val(0);
			$('.on-column-choose').children().children().children().children().val(0);
			$('.out-column-frame-choose').children().children().children().children().val(0);
		}
	});
	$('.field-product-height60x60').hide();
	$('.field-product-height80x80').hide();
	$('#product-columns_r').change(function() {
		if($(this).val()==0) {
			$('.field-product-height60x60').hide();
			$('.field-product-height80x80').hide();	
			$('#product-height60x60').val(0);
			$('#product-height80x80').val(0);
		}
		if($(this).val()==1) {
			$('.field-product-height60x60').hide();
			$('.field-product-height80x80').hide();
			$('#product-height60x60').val(0);
			$('#product-height80x80').val(0);
		}
		if($(this).val()==2) {
			$('.field-product-height80x80').hide();
			$('.field-product-height60x60').show();
			$('#product-height80x80').val(0);
		}
		if($(this).val()==3) {
			$('.field-product-height80x80').show();
			$('.field-product-height60x60').hide();
			$('#product-height60x60').val(0);
		}
	});
	$('.row-combo').hide();
	$('.row-aluteh').hide();
	$('.field-product-lamp').hide();
	$('.field-product-dop_pult').hide();
	$('#product-automation').change(function() {
		if($(this).val()==0) {
			$('.row-combo').hide();
			$('.row-aluteh').hide();
			$('.field-product-lamp').hide();
			$('.field-product-dop_pult').hide();
			$('#product-lamp').val(0);
			$('#product-dop_pult').val(0);
			$('#product-photocells').val(0);
			$('#product-antenna').val(0);
			$('#product-drive_unit').val(0);
			$('#product-combo_kit').val(0);
		}
		if($(this).val()==1) {
			$('.row-combo').hide();
			$('.row-aluteh').show();
			$('.field-product-lamp').show();
			$('.field-product-dop_pult').show();
			$('#product-drive_unit').val(0);
			$('#product-combo_kit').val(0);
			$('#product-lamp').val(0);
		}
		if($(this).val()==2) {
			$('.row-combo').show();
			$('.row-aluteh').hide();
			$('.field-product-lamp').show();
			$('.field-product-dop_pult').show();
			$('#product-lamp').val(0);
			$('#product-photocells').val(0);
			$('#product-antenna').val(0);
		}
		
	});
	
});

</script>

<script type="text/javascript"> 

$(function(){
	var w32d1 = 2500;
	var w32d2 = 3450;
	var w352d1 = 3451;
	var w352d2 = 3950;
	var w42d1 = 3951;
	var w42d2 = 4450;
	var w452d1 = 4451;
	var w452d2 = 4950;
	var w52d1 = 4951;
	var w52d2 = 5450;
	var w552d1 = 5451;
	var w552d2 = 5950;
	var w62d1 = 5951;
	var w62d2 = 6450;
	var w652d1 = 6451;
	var w652d2 = 6950;

	var d1Heightd1 = 1300;
	var d1Heightd2 = 2200;
	var d2Heightd1 = 2201;
	var d2Heightd2 = 2450;
	var d3Heightd1 = 2201;
	var d3Heightd2 = 2300;
	
	
	
	/*Под профлист*/
	function curValProflist() {
		var zap = $('#product-filling_out').val();
		var height = $('#product-height').val();
		var width = $('#product-width').val();
		var beam = $('#product-beam').val();
		var tail = $('#product-tail').val();
		var tailP = $('#product-tail_pr').val();
		var rack = $('#product-toothed_rack').val();
		var zapUstr = $('#product-locking_device').val();
		var profsheet = $('#product-profiled_sheet_s8045mm').val();
		var builtinGate = $('#product-builtin_gate').val();
		var freeGate = $('#product-type_freestanding_gate').val();
		var ramaG = $('#product-type_freestanding_gate').val();
		var betonZ = $('#product-concrete_mortgage').val();
		var pcolR = $('#product-columns_r').val();
		var freeGateFG = $('#product-filling_out_freestanding_gate').val();
		var zamok = $('#product-lock_gate').val();
		var zamokV = $('#product-wicket_lock').val();
		
		var gateOW = $('#product-gate_opening_w').val();
		var gateLW = $('#product-gate_location_w').val();
		var GateB = $('#product-builtin_gate').val();
		var rollBack = $('#product-rollback_side').val();
		var petly = $('#product-hinges').val();
		var opSide = $('#product-opening_side').val();
		var r80x80 = $('#product-height80x80').val();
		var GatePath = '';
		/*картинки калитка отдельностоящая профлист*/
		if(freeGate > 0) {
			/*var1*/
			if(freeGate == 3 && freeGateFG == 3) {
				if(petly == 1 && opSide == 1) {
					GatePath = '/img/freegate/var1/1lu.jpg';
				}
				if(petly == 1 && opSide == 2) {
					GatePath = '/img/freegate/var1/1ld.jpg';
				}
				if(petly == 2 && opSide == 1) {
					GatePath = '/img/freegate/var1/1ru.jpg';
				}
				if(petly == 2 && opSide == 2) {
					GatePath = '/img/freegate/var1/1rd.jpg';
				}
				
				if(petly == 1 && opSide == 1 && zamok == 2) {
					GatePath = '/img/freegate/var1/1lzu.jpg';
				}
				if(petly == 1 && opSide == 2 && zamok == 2) {
					GatePath = '/img/freegate/var1/1lzd.jpg';
				}
				if(petly == 2 && opSide == 1 && zamok == 2) {
					GatePath = '/img/freegate/var1/1rzu.jpg';
				}
				if(petly == 2 && opSide == 2 && zamok == 2) {
					GatePath = '/img/freegate/var1/1rzd.jpg';
				}
			}
			/*var2*/
			if(freeGate == 2 && pcolR > 0 && r80x80 < 2 && freeGateFG == 3) {
				if(petly == 1 && opSide == 1) {
					GatePath = '/img/freegate/var2/2lu.jpg';
				}
				if(petly == 1 && opSide == 2) {
					GatePath = '/img/freegate/var2/2ld.jpg';
				}
				if(petly == 2 && opSide == 1) {
					GatePath = '/img/freegate/var2/2ru.jpg';
				}
				if(petly == 2 && opSide == 2) {
					GatePath = '/img/freegate/var2/2rd.jpg';
				}
				
				if(petly == 1 && opSide == 1 && zamok == 2) {
					GatePath = '/img/freegate/var2/2lzu.jpg';
				}
				if(petly == 1 && opSide == 2 && zamok == 2) {
					GatePath = '/img/freegate/var2/2lzd.jpg';
				}
				if(petly == 2 && opSide == 1 && zamok == 2) {
					GatePath = '/img/freegate/var2/2rzu.jpg';
				}
				if(petly == 2 && opSide == 2 && zamok == 2) {
					GatePath = '/img/freegate/var2/2rzd.jpg';
				}
			}
			/*var3*/
			if(freeGate == 1 && freeGateFG == 3) {
				if(petly == 1 && opSide == 1) {
					GatePath = '/img/freegate/var3/3lu.jpg';
				}
				if(petly == 1 && opSide == 2) {
					GatePath = '/img/freegate/var3/3ld.jpg';
				}
				if(petly == 2 && opSide == 1) {
					GatePath = '/img/freegate/var3/3ru.jpg';
				}
				if(petly == 2 && opSide == 2) {
					GatePath = '/img/freegate/var3/3rd.jpg';
				}
				
				if(petly == 1 && opSide == 1 && zamok == 2) {
					GatePath = '/img/freegate/var3/3lzu.jpg';
				}
				if(petly == 1 && opSide == 2 && zamok == 2) {
					GatePath = '/img/freegate/var3/3lzd.jpg';
				}
				if(petly == 2 && opSide == 1 && zamok == 2) {
					GatePath = '/img/freegate/var3/3rzu.jpg';
				}
				if(petly == 2 && opSide == 2 && zamok == 2) {
					GatePath = '/img/freegate/var3/3rzd.jpg';
				}
			}
			/*var4*/
			if(freeGate == 2 && pcolR > 0 && r80x80 == 2 && freeGateFG == 3) {
				if(petly == 1 && opSide == 1) {
					GatePath = '/img/freegate/var4/4lu.jpg';
				}
				if(petly == 1 && opSide == 2) {
					GatePath = '/img/freegate/var4/4ld.jpg';
				}
				if(petly == 2 && opSide == 1) {
					GatePath = '/img/freegate/var4/4ru.jpg';
				}
				if(petly == 2 && opSide == 2) {
					GatePath = '/img/freegate/var4/4rd.jpg';
				}
				
				if(petly == 1 && opSide == 1 && zamok == 2) {
					GatePath = '/img/freegate/var4/4lzu.jpg';
				}
				if(petly == 1 && opSide == 2 && zamok == 2) {
					GatePath = '/img/freegate/var4/4lzd.jpg';
				}
				if(petly == 2 && opSide == 1 && zamok == 2) {
					GatePath = '/img/freegate/var4/4rzu.jpg';
				}
				if(petly == 2 && opSide == 2 && zamok == 2) {
					GatePath = '/img/freegate/var4/4rzd.jpg';
				}
			}
		}
		
		/*картинки калитка отдельностоящая 3д сетка*/
		if(freeGate > 0) {
			/*var1*/
			if(freeGate == 3 && freeGateFG == 1) {
				if(petly == 1 && opSide == 1) {
					GatePath = '/img/freegate/setka/var1/1lu.jpg';
				}
				if(petly == 1 && opSide == 2) {
					GatePath = '/img/freegate/setka/var1/1ld.jpg';
				}
				if(petly == 2 && opSide == 1) {
					GatePath = '/img/freegate/setka/var1/1ru.jpg';
				}
				if(petly == 2 && opSide == 2) {
					GatePath = '/img/freegate/setka/var1/1rd.jpg';
				}

				if(petly == 1 && opSide == 1 && zamok == 2) {
					GatePath = '/img/freegate/setka/var1/1lzu.jpg';
				}
				if(petly == 1 && opSide == 2 && zamok == 2) {
					GatePath = '/img/freegate/setka/var1/1lzd.jpg';
				}
				if(petly == 2 && opSide == 1 && zamok == 2) {
					GatePath = '/img/freegate/setka/var1/1rzu.jpg';
				}
				if(petly == 2 && opSide == 2 && zamok == 2) {
					GatePath = '/img/freegate/setka/var1/1rzd.jpg';
				}
			}
			/*var2*/
			if(freeGate == 2 && pcolR > 0 && r80x80 < 2 && freeGateFG == 1) {
				if(petly == 1 && opSide == 1) {
					GatePath = '/img/freegate/setka/var2/2lu.jpg';
				}
				if(petly == 1 && opSide == 2) {
					GatePath = '/img/freegate/setka/var2/2ld.jpg';
				}
				if(petly == 2 && opSide == 1) {
					GatePath = '/img/freegate/setka/var2/2ru.jpg';
				}
				if(petly == 2 && opSide == 2) {
					GatePath = '/img/freegate/setka/var2/2rd.jpg';
				}

				if(petly == 1 && opSide == 1 && zamok == 2) {
					GatePath = '/img/freegate/setka/var2/2lzu.jpg';
				}
				if(petly == 1 && opSide == 2 && zamok == 2) {
					GatePath = '/img/freegate/setka/var2/2lzd.jpg';
				}
				if(petly == 2 && opSide == 1 && zamok == 2) {
					GatePath = '/img/freegate/setka/var2/2rzu.jpg';
				}
				if(petly == 2 && opSide == 2 && zamok == 2) {
					GatePath = '/img/freegate/setka/var2/2rzd.jpg';
				}
			}
			/*var3*/
			if(freeGate == 1 && freeGateFG == 1) {
				if(petly == 1 && opSide == 1) {
					GatePath = '/img/freegate/setka/var3/3lu.jpg';
				}
				if(petly == 1 && opSide == 2) {
					GatePath = '/img/freegate/setka/var3/3ld.jpg';
				}
				if(petly == 2 && opSide == 1) {
					GatePath = '/img/freegate/setka/var3/3ru.jpg';
				}
				if(petly == 2 && opSide == 2) {
					GatePath = '/img/freegate/setka/var3/3rd.jpg';
				}

				if(petly == 1 && opSide == 1 && zamok == 2) {
					GatePath = '/img/freegate/setka/var3/3lzu.jpg';
				}
				if(petly == 1 && opSide == 2 && zamok == 2) {
					GatePath = '/img/freegate/setka/var3/3lzd.jpg';
				}
				if(petly == 2 && opSide == 1 && zamok == 2) {
					GatePath = '/img/freegate/setka/var3/3rzu.jpg';
				}
				if(petly == 2 && opSide == 2 && zamok == 2) {
					GatePath = '/img/freegate/setka/var3/3rzd.jpg';
				}
			}
			/*var4*/
			if(freeGate == 2 && pcolR > 0 && r80x80 == 2 && freeGateFG == 1) {
				if(petly == 1 && opSide == 1) {
					GatePath = '/img/freegate/setka/var4/4lu.jpg';
				}
				if(petly == 1 && opSide == 2) {
					GatePath = '/img/freegate/setka/var4/4ld.jpg';
				}
				if(petly == 2 && opSide == 1) {
					GatePath = '/img/freegate/setka/var4/4ru.jpg';
				}
				if(petly == 2 && opSide == 2) {
					GatePath = '/img/freegate/setka/var4/4rd.jpg';
				}

				if(petly == 1 && opSide == 1 && zamok == 2) {
					GatePath = '/img/freegate/setka/var4/4lzu.jpg';
				}
				if(petly == 1 && opSide == 2 && zamok == 2) {
					GatePath = '/img/freegate/setka/var4/4lzd.jpg';
				}
				if(petly == 2 && opSide == 1 && zamok == 2) {
					GatePath = '/img/freegate/setka/var4/4rzu.jpg';
				}
				if(petly == 2 && opSide == 2 && zamok == 2) {
					GatePath = '/img/freegate/setka/var4/4rzd.jpg';
				}
			}
		}
		
		/*картинки профлист*/
		if(zap == 3) {
			/*диапазон 1*/
			if(width >= w32d1 && width <= w42d2 && tail == 1) {
				$('.row img').attr('src', '/img/1/tr/1l1.jpg'); //по-умолчанию
				if(rollBack == 1 && zapUstr !== 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/tr/1l1.jpg');
				}

				if(rollBack == 2 && zapUstr !== 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/tr/1p1.jpg');
				}
				if(rollBack == 1 && zapUstr == 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/tr/1l1t.jpg');
				}

				if(rollBack == 2 && zapUstr == 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/tr/1p1t.jpg');
				}
				/*Сторона отката влево*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate/tr/1l2ld.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate/tr/1l2lu.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/kch-plus-gate/tr/1l2pd.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/kch-plus-gate/tr/1l2pu.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/kch-plus-gate/tr/1l2sd.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/kch-plus-gate/tr/1l2su.jpg');
				}
				/*Сторона отката вправо*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right/tr/1p2ld.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right/tr/1p2lu.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right/tr/1p2pd.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right/tr/1p2pu.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right/tr/1p2sd.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right/tr/1p2su.jpg');
				}
				/*Сторона отката влево + замок*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok/tr/1l2ldz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok/tr/1l2luz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok/tr/1l2pdz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok/tr/1l2puz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok/tr/1l2sdz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok/tr/1l2suz.jpg');
				}
				/*Сторона отката вправо + замок*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok/tr/1p2ldz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok/tr/1p2luz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok/tr/1p2pdz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok/tr/1p2puz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok/tr/1p2sdz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok/tr/1p2suz.jpg');
				}
				/*Сторона отката влево + затвор*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor/tr/1l2ldt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor/tr/1l2lut.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor/tr/1l2pdt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor/tr/1l2put.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor/tr/1l2sdt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor/tr/1l2sut.jpg');
				}
				/*Сторона отката вправо + затвор*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor/tr/1p2ldt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor/tr/1p2lut.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor/tr/1p2pdt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor/tr/1p2put.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor/tr/1p2sdt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor/tr/1p2sut.jpg');
				}
				/*Сторона отката влево + замок + затвор*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok/tr/1l2ldtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok/tr/1l2lutz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok/tr/1l2pdtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok/tr/1l2putz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok/tr/1l2sdtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok/tr/1l2sutz.jpg');
				}
				/*Сторона отката вправо + замок + затвор*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok/tr/1p2ldtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok/tr/1p2lutz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok/tr/1p2pdtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok/tr/1p2putz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok/tr/1p2sdtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok/tr/1p2sutz.jpg');
				}
			}
			if(width >= w32d1 && width <= w42d2 && tail == 2 && tailP == 2) {
				$('.row img').attr('src', '/img/1/kv/1l1.jpg'); //по-умолчанию
				if(rollBack == 1 && zapUstr !== 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/kv/1l1.jpg');
				}

				if(rollBack == 2 && zapUstr !== 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/kv/1p1.jpg');
				}
				if(rollBack == 1 && zapUstr == 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/kv/1l1t.jpg');
				}

				if(rollBack == 2 && zapUstr == 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/kv/1p1t.jpg');
				}
				/*Сторона отката влево*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate/kv/1l2ld.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate/kv/1l2lu.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/kch-plus-gate/kv/1l2pd.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/kch-plus-gate/kv/1l2pu.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/kch-plus-gate/kv/1l2sd.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/kch-plus-gate/kv/1l2su.jpg');
				}
				/*Сторона отката вправо*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right/kv/1p2ld.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right/kv/1p2lu.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right/kv/1p2pd.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right/kv/1p2pu.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right/kv/1p2sd.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right/kv/1p2su.jpg');
				}
				/*Сторона отката влево + замок*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok/kv/1l2ldz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok/kv/1l2luz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok/kv/1l2pdz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok/kv/1l2puz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok/kv/1l2sdz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok/kv/1l2suz.jpg');
				}
				/*Сторона отката вправо + замок*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok/kv/1p2ldz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok/kv/1p2luz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok/kv/1p2pdz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok/kv/1p2puz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok/kv/1p2sdz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok/kv/1p2suz.jpg');
				}
				/*Сторона отката влево + затвор*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor/kv/1l2ldt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor/kv/1l2lut.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor/kv/1l2pdt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor/kv/1l2put.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor/kv/1l2sdt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor/kv/1l2sut.jpg');
				}
				/*Сторона отката вправо + затвор*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor/kv/1p2ldt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor/kv/1p2lut.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor/kv/1p2pdt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor/kv/1p2put.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor/kv/1p2sdt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor/kv/1p2sut.jpg');
				}
				/*Сторона отката влево + замок + затвор*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok/kv/1l2ldtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok/kv/1l2lutz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok/kv/1l2pdtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok/kv/1l2putz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok/kv/1l2sdtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok/kv/1l2sutz.jpg');
				}
				/*Сторона отката вправо + замок + затвор*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok/kv/1p2ldtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok/kv/1p2lutz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok/kv/1p2pdtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok/kv/1p2putz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok/kv/1p2sdtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok/kv/1p2sutz.jpg');
				}
			}
			/*диапазон 2*/
			if(width >= w452d1 && width <= w552d2 && tail == 2 && tailP == 2) {
				$('.row img').attr('src', '/img/1/kv/2l1.jpg'); //по-умолчанию
				if(rollBack == 1 && zapUstr !== 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/kv/2l1.jpg');
				}

				if(rollBack == 2 && zapUstr !== 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/kv/2p1.jpg');
				}
				if(rollBack == 1 && zapUstr == 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/kv/2l1t.jpg');
				}

				if(rollBack == 2 && zapUstr == 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/kv/2p1t.jpg');
				}
				/*Сторона отката влево*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-2/kv/2l2ld.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-2/kv/2l2lu.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-2/kv/2l2pd.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-2/kv/2l2pu.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-2/kv/2l2sd.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-2/kv/2l2su.jpg');
				}
				/*Сторона отката вправо*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-2/kv/2p2ld.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-2/kv/2p2lu.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-2/kv/2p2pd.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-2/kv/2p2pu.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-2/kv/2p2sd.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-2/kv/2p2su.jpg');
				}
				/*Сторона отката влево + замок*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok-2/kv/2l2ldz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok-2/kv/2l2luz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok-2/kv/2l2pdz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok-2/kv/2l2puz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok-2/kv/2l2sdz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok-2/kv/2l2suz.jpg');
				}
				/*Сторона отката вправо + замок*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok-2/kv/2p2ldz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok-2/kv/2p2luz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok-2/kv/2p2pdz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok-2/kv/2p2puz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok-2/kv/2p2sdz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok-2/kv/2p2suz.jpg');
				}
				/*Сторона отката влево + затвор*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-2/kv/2l2ldt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-2/kv/2l2lut.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-2/kv/2l2pdt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-2/kv/2l2put.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-2/kv/2l2sdt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-2/kv/2l2sut.jpg');
				}
				/*Сторона отката вправо + затвор*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-2/kv/2p2ldt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-2/kv/2p2lut.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-2/kv/2p2pdt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-2/kv/2p2put.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-2/kv/2p2sdt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-2/kv/2p2sut.jpg');
				}
				/*Сторона отката влево + замок + затвор*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok-2/kv/2l2ldtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok-2/kv/2l2lutz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok-2/kv/2l2pdtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok-2/kv/2l2putz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok-2/kv/2l2sdtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok-2/kv/2l2sutz.jpg');
				}
				/*Сторона отката вправо + замок + затвор*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok-2/kv/2p2ldtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok-2/kv/2p2lutz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok-2/kv/2p2pdtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok-2/kv/2p2putz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok-2/kv/2p2sdtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok-2/kv/2p2sutz.jpg');
				}
			}
			if(width >= w452d1 && width <= w552d2 && tail == 1) {
				$('.row img').attr('src', '/img/1/tr/2l1.jpg'); //по-умолчанию
				if(rollBack == 1 && zapUstr !== 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/tr/2l1.jpg');
				}

				if(rollBack == 2 && zapUstr !== 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/tr/2p1.jpg');
				}
				if(rollBack == 1 && zapUstr == 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/tr/2l1t.jpg');
				}

				if(rollBack == 2 && zapUstr == 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/tr/2p1t.jpg');
				}
				/*Сторона отката влево*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-2/tr/2l2ld.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-2/tr/2l2lu.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-2/tr/2l2pd.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-2/tr/2l2pu.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-2/tr/2l2sd.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-2/tr/2l2su.jpg');
				}
				/*Сторона отката вправо*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-2/tr/2p2ld.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-2/tr/2p2lu.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-2/tr/2p2pd.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-2/tr/2p2pu.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-2/tr/2p2sd.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-2/tr/2p2su.jpg');
				}
				/*Сторона отката влево + замок*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok-2/tr/2l2ldz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok-2/tr/2l2luz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok-2/tr/2l2pdz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok-2/tr/2l2puz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok-2/tr/2l2sdz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok-2/tr/2l2suz.jpg');
				}
				/*Сторона отката вправо + замок*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok-2/tr/2p2ldz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok-2/tr/2p2luz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok-2/tr/2p2pdz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok-2/tr/2p2puz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok-2/tr/2p2sdz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok-2/tr/2p2suz.jpg');
				}
				/*Сторона отката влево + затвор*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-2/tr/2l2ldt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-2/tr/2l2lut.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-2/tr/2l2pdt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-2/tr/2l2put.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-2/tr/2l2sdt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-2/tr/2l2sut.jpg');
				}
				/*Сторона отката вправо + затвор*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-2/tr/2p2ldt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-2/tr/2p2lut.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-2/tr/2p2pdt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-2/tr/2p2put.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-2/tr/2p2sdt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-2/tr/2p2sut.jpg');
				}
				/*Сторона отката влево + замок + затвор*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok-2/tr/2l2ldtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok-2/tr/2l2lutz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok-2/tr/2l2pdtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok-2/tr/2l2putz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok-2/tr/2l2sdtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok-2/tr/2l2sutz.jpg');
				}
				/*Сторона отката вправо + замок + затвор*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok-2/tr/2p2ldtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok-2/tr/2p2lutz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok-2/tr/2p2pdtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok-2/tr/2p2putz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok-2/tr/2p2sdtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok-2/tr/2p2sutz.jpg');
				}
			}
			/*диапазон 3*/
			if(width >= w62d1 && width <= w652d2 && tail == 2 && tailP == 2) {
				$('.row img').attr('src', '/img/1/kv/1l1.jpg'); //по-умолчанию
				if(rollBack == 1 && zapUstr !== 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/kv/3l1.jpg');
				}

				if(rollBack == 2 && zapUstr !== 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/kv/3p1.jpg');
				}
				if(rollBack == 1 && zapUstr == 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/kv/3l1t.jpg');
				}

				if(rollBack == 2 && zapUstr == 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/kv/3p1t.jpg');
				}
				/*Сторона отката влево*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-3/3l2ld.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-3/3l2lu.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-3/3l2pd.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-3/3l2pu.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-3/3l2sd.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-3/3l2su.jpg');
				}
				/*Сторона отката вправо*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-3/3p2ld.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-3/3p2lu.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-3/3p2pd.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-3/3p2pu.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-3/3p2sd.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-3/3p2su.jpg');
				}
				/*Сторона отката влево + замок*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok-3/3l2ldz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok-3/3l2luz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok-3/3l2pdz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok-3/3l2puz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok-3/3l2sdz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zamok-3/3l2suz.jpg');
				}
				/*Сторона отката вправо + замок*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok-3/3p2ldz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok-3/3p2luz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok-3/3p2pdz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok-3/3p2puz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok-3/3p2sdz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zamok-3/3p2suz.jpg');
				}
				/*Сторона отката влево + затвор*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-3/3l2ldt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-3/3l2lut.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-3/3l2pdt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-3/3l2put.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-3/3l2sdt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-3/3l2sut.jpg');
				}
				/*Сторона отката вправо + затвор*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-3/3p2ldt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-3/3p2lut.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-3/3p2pdt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-3/3p2put.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-3/3p2sdt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-3/3p2sut.jpg');
				}
				/*Сторона отката влево + замок + затвор*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok-3/3l2ldtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok-3/3l2lutz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok-3/3l2pdtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok-3/3l2putz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok-3/3l2sdtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-zatvor-zamok-3/3l2sutz.jpg');
				}
				/*Сторона отката вправо + замок + затвор*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok-3/3p2ldtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok-3/3p2lutz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok-3/3p2pdtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok-3/3p2putz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok-3/3p2sdtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/kch-plus-gate-right-zatvor-zamok-3/3p2sutz.jpg');
				}
			}
		}
		/*картинки сетка*/
		if(zap == 1) {
			/*диапазон 1*/
			if(width >= w32d1 && width <= w42d2 && tail == 1) {
				$('.row img').attr('src', '/img/1/setka/tr/1l1.jpg'); //по-умолчанию
				if(rollBack == 1 && zapUstr == 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/setka/tr/1l1t.jpg');
				}

				if(rollBack == 2 && zapUstr == 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/setka/tr/1p1t.jpg');
				}
				/*Сторона отката влево*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate/tr/1l2ld.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate/tr/1l2lu.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate/tr/1l2pd.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate/tr/1l2pu.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate/tr/1l2sd.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate/tr/1l2su.jpg');
				}
				/*Сторона отката вправо*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right/tr/1p2ld.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right/tr/1p2lu.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right/tr/1p2pd.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right/tr/1p2pu.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right/tr/1p2sd.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right/tr/1p2su.jpg');
				}
				/*Сторона отката влево + замок*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok/tr/1l2ldz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok/tr/1l2luz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok/tr/1l2pdz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok/tr/1l2puz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok/tr/1l2sdz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok/tr/1l2suz.jpg');
				}
				/*Сторона отката вправо + замок*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok/tr/1p2ldz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok/tr/1p2luz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok/tr/1p2pdz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok/tr/1p2puz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok/tr/1p2sdz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok/tr/1p2suz.jpg');
				}
				/*Сторона отката влево + затвор*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor/tr/1l2ldt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor/tr/1l2lut.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor/tr/1l2pdt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor/tr/1l2put.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor/tr/1l2sdt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor/tr/1l2sut.jpg');
				}
				/*Сторона отката вправо + затвор*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor/tr/1p2ldt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor/tr/1p2lut.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor/tr/1p2pdt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor/tr/1p2put.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor/tr/1p2sdt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor/tr/1p2sut.jpg');
				}
				/*Сторона отката влево + замок + затвор*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok/tr/1l2ldtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok/tr/1l2lutz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok/tr/1l2pdtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok/tr/1l2putz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok/tr/1l2sdtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok/tr/1l2sutz.jpg');
				}
				/*Сторона отката вправо + замок + затвор*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok/tr/1p2ldtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok/tr/1p2lutz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok/tr/1p2pdtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok/tr/1p2putz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok/tr/1p2sdtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok/tr/1p2sutz.jpg');
				}
			}
			if(width >= w32d1 && width <= w42d2 && tail == 2 && tailP == 2) {
				$('.row img').attr('src', '/img/1/setka/kv/1l1.jpg'); //по-умолчанию
				if(rollBack == 1 && zapUstr == 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/setka/kv/1l1t.jpg');
				}

				if(rollBack == 2 && zapUstr == 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/setka/kv/1p1t.jpg');
				}
				/*Сторона отката влево*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate/kv/1l2ld.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate/kv/1l2lu.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate/kv/1l2pd.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate/kv/1l2pu.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate/kv/1l2sd.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate/kv/1l2su.jpg');
				}
				/*Сторона отката вправо*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right/kv/1p2ld.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right/kv/1p2lu.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right/kv/1p2pd.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right/kv/1p2pu.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right/kv/1p2sd.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right/kv/1p2su.jpg');
				}
				/*Сторона отката влево + замок*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok/kv/1l2ldz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok/kv/1l2luz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok/kv/1l2pdz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok/kv/1l2puz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok/kv/1l2sdz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok/kv/1l2suz.jpg');
				}
				/*Сторона отката вправо + замок*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok/kv/1p2ldz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok/kv/1p2luz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok/kv/1p2pdz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok/kv/1p2puz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok/kv/1p2sdz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok/kv/1p2suz.jpg');
				}
				/*Сторона отката влево + затвор*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor/kv/1l2ldt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor/kv/1l2lut.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor/kv/1l2pdt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor/kv/1l2put.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor/kv/1l2sdt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor/kv/1l2sut.jpg');
				}
				/*Сторона отката вправо + затвор*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor/kv/1p2ldt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor/kv/1p2lut.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor/kv/1p2pdt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor/kv/1p2put.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor/kv/1p2sdt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor/kv/1p2sut.jpg');
				}
				/*Сторона отката влево + замок + затвор*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok/kv/1l2ldtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok/kv/1l2lutz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok/kv/1l2pdtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok/kv/1l2putz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok/kv/1l2sdtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok/kv/1l2sutz.jpg');
				}
				/*Сторона отката вправо + замок + затвор*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok/kv/1p2ldtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok/kv/1p2lutz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok/kv/1p2pdtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok/kv/1p2putz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok/kv/1p2sdtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok/kv/1p2sutz.jpg');
				}
			}
			/*диапазон 2*/
			if(width >= w452d1 && width <= w552d2 && tail == 2 && tailP == 2) {
				$('.row img').attr('src', '/img/1/setka/kv/2l1.jpg'); //по-умолчанию
				if(rollBack == 1 && zapUstr == 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/setka/kv/2l1t.jpg');
				}

				if(rollBack == 2 && zapUstr == 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/setka/kv/2p1t.jpg');
				}
				/*Сторона отката влево*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-2/kv/2l2ld.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-2/kv/2l2lu.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-2/kv/2l2pd.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-2/kv/2l2pu.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-2/kv/2l2sd.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-2/kv/2l2su.jpg');
				}
				/*Сторона отката вправо*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-2/kv/2p2ld.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-2/kv/2p2lu.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-2/kv/2p2pd.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-2/kv/2p2pu.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-2/kv/2p2sd.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-2/kv/2p2su.jpg');
				}
				/*Сторона отката влево + замок*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok-2/kv/2l2ldz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok-2/kv/2l2luz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok-2/kv/2l2pdz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok-2/kv/2l2puz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok-2/kv/2l2sdz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok-2/kv/2l2suz.jpg');
				}
				/*Сторона отката вправо + замок*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok-2/kv/2p2ldz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok-2/kv/2p2luz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok-2/kv/2p2pdz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok-2/kv/2p2puz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok-2/kv/2p2sdz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok-2/kv/2p2suz.jpg');
				}
				/*Сторона отката влево + затвор*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-2/kv/2l2ldt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-2/kv/2l2lut.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-2/kv/2l2pdt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-2/kv/2l2put.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-2/kv/2l2sdt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-2/kv/2l2sut.jpg');
				}
				/*Сторона отката вправо + затвор*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-2/kv/2p2ldt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-2/kv/2p2lut.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-2/kv/2p2pdt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-2/kv/2p2put.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-2/kv/2p2sdt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-2/kv/2p2sut.jpg');
				}
				/*Сторона отката влево + замок + затвор*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok-2/kv/2l2ldtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok-2/kv/2l2lutz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok-2/kv/2l2pdtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok-2/kv/2l2putz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok-2/kv/2l2sdtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok-2/kv/2l2sutz.jpg');
				}
				/*Сторона отката вправо + замок + затвор*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok-2/kv/2p2ldtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok-2/kv/2p2lutz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok-2/kv/2p2pdtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok-2/kv/2p2putz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok-2/kv/2p2sdtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok-2/kv/2p2sutz.jpg');
				}
			}
			if(width >= w452d1 && width <= w552d2 && tail == 1) {
				$('.row img').attr('src', '/img/1/setka/tr/2l1.jpg'); //по-умолчанию
				if(rollBack == 1 && zapUstr == 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/setka/tr/2l1t.jpg');
				}

				if(rollBack == 2 && zapUstr == 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/setka/tr/2p1t.jpg');
				}
				/*Сторона отката влево*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-2/tr/2l2ld.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-2/tr/2l2lu.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-2/tr/2l2pd.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-2/tr/2l2pu.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-2/tr/2l2sd.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-2/tr/2l2su.jpg');
				}
				/*Сторона отката вправо*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-2/tr/2p2ld.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-2/tr/2p2lu.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-2/tr/2p2pd.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-2/tr/2p2pu.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-2/tr/2p2sd.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-2/tr/2p2su.jpg');
				}
				/*Сторона отката влево + замок*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok-2/tr/2l2ldz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok-2/tr/2l2luz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok-2/tr/2l2pdz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok-2/tr/2l2puz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok-2/tr/2l2sdz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok-2/tr/2l2suz.jpg');
				}
				/*Сторона отката вправо + замок*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok-2/tr/2p2ldz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok-2/tr/2p2luz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok-2/tr/2p2pdz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok-2/tr/2p2puz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok-2/tr/2p2sdz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok-2/tr/2p2suz.jpg');
				}
				/*Сторона отката влево + затвор*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-2/tr/2l2ldt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-2/tr/2l2lut.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-2/tr/2l2pdt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-2/tr/2l2put.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-2/tr/2l2sdt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-2/tr/2l2sut.jpg');
				}
				/*Сторона отката вправо + затвор*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-2/tr/2p2ldt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-2/tr/2p2lut.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-2/tr/2p2pdt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-2/tr/2p2put.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-2/tr/2p2sdt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-2/tr/2p2sut.jpg');
				}
				/*Сторона отката влево + замок + затвор*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok-2/tr/2l2ldtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok-2/tr/2l2lutz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok-2/tr/2l2pdtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok-2/tr/2l2putz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok-2/tr/2l2sdtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok-2/tr/2l2sutz.jpg');
				}
				/*Сторона отката вправо + замок + затвор*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok-2/tr/2p2ldtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok-2/tr/2p2lutz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok-2/tr/2p2pdtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok-2/tr/2p2putz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok-2/tr/2p2sdtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok-2/tr/2p2sutz.jpg');
				}
			}
			/*диапазон 3*/
			if(width >= w62d1 && width <= w652d2 && tail == 2 && tailP == 2) {
				$('.row img').attr('src', '/img/1/setka/kv/3l1.jpg'); //по-умолчанию
				if(rollBack == 1 && zapUstr == 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/setka/kv/3l1t.jpg');
				}

				if(rollBack == 2 && zapUstr == 2 &&  GateB !== 2) {
					$('.row img').attr('src', '/img/1/setka/kv/3p1t.jpg');
				}
				/*Сторона отката влево*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-3/3l2ld.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-3/3l2lu.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-3/3l2pd.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-3/3l2pu.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-3/3l2sd.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-3/3l2su.jpg');
				}
				/*Сторона отката вправо*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-3/3p2ld.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-3/3p2lu.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-3/3p2pd.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-3/3p2pu.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-3/3p2sd.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-3/3p2su.jpg');
				}
				/*Сторона отката влево + замок*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok-3/3l2ldz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok-3/3l2luz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok-3/3l2pdz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok-3/3l2puz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok-3/3l2sdz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zamok-3/3l2suz.jpg');
				}
				/*Сторона отката вправо + замок*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok-3/3p2ldz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok-3/3p2luz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok-3/3p2pdz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok-3/3p2puz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok-3/3p2sdz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zamok-3/3p2suz.jpg');
				}
				/*Сторона отката влево + затвор*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-3/3l2ldt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-3/3l2lut.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-3/3l2pdt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-3/3l2put.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-3/3l2sdt.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-3/3l2sut.jpg');
				}
				/*Сторона отката вправо + затвор*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-3/3p2ldt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-3/3p2lut.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-3/3p2pdt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-3/3p2put.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-3/3p2sdt.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-3/3p2sut.jpg');
				}
				/*Сторона отката влево + замок + затвор*/
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok-3/3l2ldtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok-3/3l2lutz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok-3/3l2pdtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok-3/3l2putz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok-3/3l2sdtz.jpg');
				}
				if(rollBack == 1 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-zatvor-zamok-3/3l2sutz.jpg');
				}
				/*Сторона отката вправо + замок + затвор*/
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok-3/3p2ldtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 2 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok-3/3p2lutz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok-3/3p2pdtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 3 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok-3/3p2putz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 2 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok-3/3p2sdtz.jpg');
				}
				if(rollBack == 2 && GateB == 2 && gateOW == 1 && gateLW == 1 && zapUstr == 2 && zamokV == 2) {
					$('.row img').attr('src', '/img/1/setka/kch-plus-gate-right-zatvor-zamok-3/3p2sutz.jpg');
				}
			}
		}
		
		/*console.log('gateOW'+gateOW);
		console.log('gateLW'+gateLW);
		console.log('GateB'+GateB);
		console.log('rollBack'+rollBack);
		console.log($('.row img').attr('src'));*/
		
		var summFreeGateF = 0;
		var summ = 0;
		var zapRes = 0;
		var resRack = 0;
		var rG = 0;
		var bZ = 0;
		var colG = 0;
		var heightFG = 0;
		
		if(zamok == 2) {
			var zamSum = 2000;
		} else {
			var zamSum = 0;
		}
		if(zamokV == 2) {
			var zamSumV = 2000;
		} else {
			var zamSumV = 0;
		}
		if(zap == 1) {
			var zapRes = ZDsetka(height, width, beam, tail, tailP, freeGate);
		}
		if(zap == 2) {
			var zapRes = reshetka(height, width, beam, tail, tailP, freeGate);
		}
		if(zap == 3) {
			var zapRes = proflist(height, width, beam, tail, tailP, freeGate);
		}
		
		if(rack == 1 && width>0) {
			var curWidth = $('#product-width').val();
			curWidth = parseInt(curWidth)+500;
			curWidth = Math.ceil(curWidth / 1000) * 1000;
			var resRack = (parseInt(curWidth)/1000)*500;
			var resRC = parseInt(curWidth)/1000;
			var countRack = $('#product-toothed_rack_count').val(resRC);
			console.log(curWidth);
		}
		if(rack == 2 && width>0) {	
			var curWidth = $('#product-width').val();
			curWidth = parseInt(curWidth)+500;
			curWidth = Math.ceil(curWidth / 1000) * 1000;
			console.log(curWidth);
			var countRack = $('#product-toothed_rack_count').val();
			var resRack = countRack*500;
		}
		if(builtinGate == 2) {
			var summ = 0;
			if(height >= d1Heightd1 && height <= d1Heightd2) {
				var summ = 5200;	
			}
			if(height >= d2Heightd1 && height <= d2Heightd2) {
				var summ = 5200+5200/100*10;
			}
		}
		if(betonZ == 2) {
			var bZ = beton(width);
		}
		var shV = shveller();
		var svaiC = svai();
		if(freeGate == 1) {
			var heightFG = $('#product-frame_height').val();		
		}
		if(freeGate == 2) {
			var heightFG = $('#product-gate_height').val();		
		}
		if(freeGate == 3) {
			var heightFG = $('#product-height_without_cf').val();		
		}
		if(ramaG == 1) {
			var rG = ramaGate(heightFG);
		}
		if(pcolR > 0) {
			var colG = columnGate(heightFG);
		}
		summFreeGateF = summFreeGate(freeGateFG, heightFG);
		zapUstr = zapUstrF(zapUstr);
		//profsheet = profsheetF(profsheet);
		var curWidth = $('#product-width').val();
		var curHeight = $('#product-height').val();
		var koefPF = 0;
		var sumPF = 0;
		var hList = 0;
		var packSheet = 0;
		var samrez = 0;
		var d = 0;
		if(curWidth > 0 && curHeight > 0) {
			if(profsheet == 1 && $('#product-compl_proflist').val() == 0) {
				var curSumP = 0;
			}
			if(profsheet == 1 && $('#product-compl_proflist').val()>0) {
				koefPF = 475;
				var beam = $('#product-beam').val();
				if(beam == 1) {
					beam = 60;	
				}
				if(beam == 2) {
					beam = 85;	
				}
				var profile = 30;
				var vnSize = parseInt(curHeight)-beam-profile-5;
				var hList = Math.floor(vnSize/10)*10;
				hList = hList/1000;
				var zap12 = $('#product-compl_proflist').val();
				if(zap12 == 1) {
					curWidth = parseInt(curWidth)-110;
					var d = curWidth/1150;
					d = Math.ceil(d);
					var samrez = Math.ceil(18*d);
					samrez = samrez*2.5;
					sumPF = 475*hList*d;
					var curSumP = Math.ceil(475*hList*d+samrez+hList*250);
				}
				if(zap12 == 2) {
					curWidth = parseInt(curWidth-110)*2+50;
					var d = curWidth/1150;
					d = Math.ceil(d);
					var samrez = Math.ceil(18*d);
					samrez = samrez*2.5;
					sumPF = 475*hList*d;
					var curSumP = Math.ceil(475*hList*d+samrez+hList*250);
				}
				var packSheet = hList*250;
				console.log(hList);
				console.log(curSumP);
			}
			if(profsheet == 2) {
				koefPF = 616;
				curWidth = parseInt(curWidth)-110;
				var d = curWidth/1150;
				d = Math.ceil(d);
				var beam = $('#product-beam').val();
				if(beam == 1) {
					beam = 60;	
				}
				if(beam == 2) {
					beam = 85;	
				}
				var profile = 30;
				var vnSize = parseInt(curHeight)-beam-profile-5;
				var hList = Math.floor(vnSize/10)*10;
				hList = hList/1000;
				var samrez = Math.ceil(18*d);
				samrez = samrez*2.5;
				var zap12 = $('#product-compl_proflist').val();
				var packSheet = hList*250;
				sumPF = 616*hList*d;
				var curSumP = Math.ceil(616*hList*d+samrez+hList*250);
				console.log(hList);
				console.log(curSumP);
			}
			if(profsheet == 0) {
				var curSumP = 0;
			}
		} else {
			var curSumP = 0;
		}
		var lampCombo = 0;
		var antennaCombo = 0;
		var pultCombo = 0;
		var lampAl = 0;
		var photoAl = 0;
		var pultAl = 0;
		var automatSummCombo = 0;
		var automatSummAl = 0;
		var automat = $('#product-automation').val();
		if(automat == 1) {
			automatSummAl = automatPriceA();
			if($('#product-lamp').val() == 2) {
				lampAl = 1947;
			}
			if($('#product-photocells').val() == 2) {
				photoAl = 1771;
			}
			
			if($('#product-dop_pult').val() > 0) {
				var cp = $('#product-dop_pult').val();
				pultAl = 1045*cp;
			}
		}
		if(automat == 2) {
			automatSummCombo = automatPriceC();
			if($('#product-lamp').val() == 2) {
				lampCombo = 1860;
			}
			if($('#product-antenna').val() == 2) {
				antennaCombo = 990;
			}
			if($('#product-dop_pult').val() > 0) {
				var cp = $('#product-dop_pult').val();
				pultCombo = 1320*cp;
			}
		}
		var imgPath = $('.row img').attr('src');
		var cameD = $('.cameD').val();
		var alutehD = $('.alutehD').val();
		var reikaD = $('.reikaD').val();
		var otherD = $('.otherD').val();
		if(cameD > 0) {
			automatSummCombo = Math.round(automatSummCombo - (automatSummCombo/100*cameD));
			lampCombo = Math.round(lampCombo - (lampCombo/100*cameD));
			antennaCombo = Math.round(antennaCombo - (antennaCombo/100*cameD));
			pultCombo = Math.round(pultCombo - (pultCombo/100*cameD));
		}
		if(alutehD > 0) {
			automatSummAl = Math.round(automatSummAl - (automatSummAl/100*alutehD));
			lampAl = Math.round(lampAl - (lampAl/100*alutehD));
			photoAl = Math.round(photoAl - (photoAl/100*alutehD));
			pultAl = Math.round(pultAl - (pultAl/100*alutehD));
		}
		if(reikaD > 0) {
			resRack = Math.round(resRack - (resRack/100*reikaD));
		}
		if(otherD > 0) {
			zapRes = Math.round(zapRes - (zapRes/100*otherD));
			summ = Math.round(summ - (summ/100*otherD));
			zapUstr = Math.round(zapUstr - (zapUstr/100*otherD));
			curSumP = Math.round(curSumP - (curSumP/100*otherD));
			rG = Math.round(rG - (rG/100*otherD));
			bZ = Math.round(bZ - (bZ/100*otherD));
			shV = Math.round(shV - (shV/100*otherD));
			svaiC = Math.round(svaiC - (svaiC/100*otherD));
			colG = Math.round(colG - (colG/100*otherD));
			summFreeGateF = Math.round(summFreeGateF - (summFreeGateF/100*otherD));
			zamSum = Math.round(zamSum - (zamSum/100*otherD));
			zamSumV = Math.round(zamSumV - (zamSumV/100*otherD));
			packSheet = Math.round(packSheet - (packSheet/100*otherD));
			sumPF = Math.round(sumPF - (sumPF/100*otherD));
		}
		
		var allResult = (zapRes+summ+resRack+zapUstr+curSumP+rG+bZ+shV+svaiC+colG+summFreeGateF+zamSum+zamSumV
						+automatSummCombo+automatSummAl+lampCombo+antennaCombo+pultCombo+lampAl+photoAl+pultAl);
		var arrPrices = {'zapRes': zapRes, 'summ': summ, 'resRack': resRack, 'zapUstr': zapUstr, 'curSumP': curSumP,
						'rG': rG, 'bZ': bZ, 'shV': shV, 'svaiC': svaiC, 'colG': colG, 'summFreeGateF': summFreeGateF,
						'zamSum': zamSum, 'zamSumV': zamSumV, 'automatSummCombo': automatSummCombo, 'automatSummAl': automatSummAl,
						'lampCombo': lampCombo, 'antennaCombo': antennaCombo, 'pultCombo': pultCombo, 'lampAl': lampAl,
						'photoAl': photoAl, 'pultAl': pultAl, 'imgPath': imgPath, 'd': d, 'samrez': samrez, 'packSheet': packSheet,
						'koefPF': koefPF, 'sumPF': sumPF, 'hList': hList, 'GatePath': GatePath};
		var jsonStr = JSON.stringify(arrPrices);
		$('#product-arr_prices').val(jsonStr);
		$('#productSum').val(allResult);
		$('#product-price').val(allResult);
		
	}
	
	function automatPriceC() {
		var automatPriceCom = 0;
		if($('#product-combo_kit').val() > 0) {
			if($('#product-combo_kit').val() == 1) {
				automatPriceCom = 24306;
			}
			if($('#product-combo_kit').val() == 2) {
				automatPriceCom = 25694;
			}
		}
		return automatPriceCom;
	}
	function automatPriceA() {
		var automatPriceAlu = 0;
		if($('#product-drive_unit').val() > 0) {
			if($('#product-drive_unit').val() == 1) {
				var automatPriceAlu = 17622;
			}
			if($('#product-drive_unit').val() == 2) {
				var automatPriceAlu = 20438;
			}
			if($('#product-drive_unit').val() == 3) {
				var automatPriceAlu = 24926;
			}
		}
		return automatPriceAlu;
	}
	function zapUstrF(zapUstr) {
		if(zapUstr == 2) {
			var curSum = 500;
		} else {
			var curSum = 0;
		}
		return curSum;
	}
	function profsheetF(profsheet) {
		var curWidth = $('#product-width').val();
		var curHeight = $('#product-height').val();
		if(curWidth > 0 && curHeight > 0) {
			if(profsheet == 1 && $('#product-compl_proflist').val() == 0) {
				var curSumP = 0;
			}
			if(profsheet == 1 && $('#product-compl_proflist').val()>0) {
				var beam = $('#product-beam').val();
				if(beam == 1) {
					beam = 60;	
				}
				if(beam == 2) {
					beam = 85;	
				}
				var profile = 30;
				var vnSize = parseInt(curHeight)-beam-profile-5;
				var hList = Math.floor(vnSize/10)*10;
				hList = hList/1000;
				var zap12 = $('#product-compl_proflist').val();
				if(zap12 == 1) {
					curWidth = parseInt(curWidth)-110;
					var d = curWidth/1150;
					d = Math.ceil(d);
					var samrez = Math.ceil(18*d);
					samrez = samrez*2.5;
					var curSumP = Math.ceil(475*hList*d+samrez+hList*250);
				}
				if(zap12 == 2) {
					curWidth = parseInt(curWidth-110)*2+50;
					var d = curWidth/1150;
					d = Math.ceil(d);
					var samrez = Math.ceil(18*d);
					samrez = samrez*2.5;
					var curSumP = Math.ceil(475*hList*d+samrez+hList*250);
				}
				console.log(hList);
				console.log(curSumP);
			}
			if(profsheet == 2) {
				curWidth = parseInt(curWidth)-110;
				var d = curWidth/1150;
				d = Math.ceil(d);
				var beam = $('#product-beam').val();
				if(beam == 1) {
					beam = 60;	
				}
				if(beam == 2) {
					beam = 85;	
				}
				var profile = 30;
				var vnSize = parseInt(curHeight)-beam-profile-5;
				var hList = Math.floor(vnSize/10)*10;
				hList = hList/1000;
				var samrez = Math.ceil(18*d);
				samrez = samrez*2.5;
				var zap12 = $('#product-compl_proflist').val();
				var curSumP = Math.ceil(616*hList*d+samrez+hList*250);
				console.log(hList);
				console.log(curSumP);
			}
			if(profsheet == 0) {
				var curSumP = 0;
			}
		} else {
			var curSumP = 0;
		}
		return curSumP;
	}
	
	function ramaGate(heightFG) {
		var frameP = $('#product-frame_profile').val();
		console.log(frameP);
		if(frameP > 0) {
			if(heightFG >= d1Heightd1 && heightFG <= d1Heightd2) {
				if(frameP == 1) {
					var framePV = 2000;	
				}
				if(frameP == 2) {
					var framePV = 2400;	
				}
				if(frameP == 3) {
					var framePV = 2800;	
				}
			}
			if(heightFG >= d2Heightd1 && heightFG <= d2Heightd2) {
				if(frameP == 1) {
					var framePV = 2000+2000/100*10;	
				}
				if(frameP == 2) {
					var framePV = 2400+2400/100*10;	
				}
				if(frameP == 3) {
					var framePV = 2800+2800/100*10;	
				}
			}
			if(heightFG < 1) {
				framePV = 0;	
			}
		} else {
			framePV = 0;
		}
		return framePV;
	}
	
	function beton(width) {
		var zapV = 0;
		if(width >= w32d1 && width <= w32d2) {
			var zapV = 3100;
		}
		if(width >= w352d1 && width <= w352d2) {
			var zapV = 3300;
		}
		if(width >= w42d1 && width <= w42d2) {
			var zapV = 3500;
		}
		if(width >= w452d1 && width <= w452d2) {
			var zapV = 3600;
		}
		if(width >= w52d1 && width <= w52d2) {
			var zapV = 3900;
		}
		if(width >= w552d1 && width <= w552d2) {
			var zapV = 4300;
		}
		if(width >= w62d1 && width <= w62d2) {
			var zapV = 4500;
		}
		if(width >= w652d1 && width <= w652d2) {
			var zapV = 4700;
		}
		
		return zapV;
	}
	
	function shveller() {
		var shv = $('#product-channel_p12').val();
		if(shv > 0) {
			var resShv = shv*850;
		} else {
			var resShv = 0;
		}
		
		return resShv;
	}
	
	function svai() {
		
		var sv57 = $('#product-pile_57x2000mm').val();
		var sv76 = $('#product-pile_76x2000mm').val();
		var sv89 = $('#product-pile_89x2000mm').val();
		var sv108 = $('#product-pile_108x2000mm').val();
		var og57 = $('#product-heading_57').val();
		var og76 = $('#product-heading_76').val();
		var og89 = $('#product-heading_89').val();
		var og108 = $('#product-heading_108').val();
		if(sv57>0) {
		   var Rsv57 = sv57*938;
		} else {
			var Rsv57 = 0;
		}
		if(sv76>0) {
		   var Rsv76 = sv76*1290;
		} else {
			var Rsv76 = 0;
		}
		if(sv89>0) {
		   var Rsv89 = sv89*1380;
		} else {
			var Rsv89 = 0;
		}
		if(sv108 > 0) {
		   var Rsv108 = sv108*1750;
		} else {
			var Rsv108 = 0;
		}
		if(og57>0) {
		    var Rog57 = og57*230;
		} else {
			var Rog57 = 0;
		}
		if(og76>0) {
		    var Rog76 = og76*250;
		} else {
			var Rog76 = 0;
		}
		if(og89>0) {
		    var Rog89 = og89*250;
		} else {
			var Rog89 = 0;
		}
		if(og108>0) {
		    var Rog108 = og108*280;
		} else {
			var Rog108 = 0;
		}
		
		var resSvai = Rsv57+Rsv76+Rsv89+Rsv108+Rog57+Rog76+Rog89+Rog108;
		return resSvai;
		
	}
	
	function columnGate(heightFG) {
		var frameP = $('#product-columns_r').val();
		var framePG60 = $('#product-height60x60').val();
		var framePG80 = $('#product-height80x80').val();
		var framePV = 0;
		if(heightFG >= d1Heightd1 && heightFG <= d1Heightd2) {
			if(frameP == 1) {
				var framePV = 920;	
			}
			if(frameP == 2) {
				if(framePG60 == 1) {
					var framePV = 1210;	
				}
				if(framePG60 == 2) {
					var framePV = 1742;	
				}
			}
			if(frameP == 3) {
				if(framePG80 == 1) {
					var framePV = 1762;	
				}
				if(framePG80 == 2) {
					var framePV = 2546;	
				}	
			}
		}
		if(heightFG >= d2Heightd1 && heightFG <= d2Heightd2) {
			if(frameP == 1) {
				var framePV = 920+920/100*10;		
			}
			if(frameP == 2) {
				if(framePG60 == 1) {
					var framePV = 1210+1210/100*10;		
				}
				if(framePG60 == 2) {
					var framePV = 1742;		
				}
			}
			if(frameP == 3) {
				if(framePG80 == 1) {
					var framePV = 1762+1762/100*10;	
				}
				if(framePG80 == 2) {
					var framePV = 2546;	
				}	
			}
		}
		if(heightFG == 0) {
			var framePV = 0;	
		}
		return framePV;
	}
	function summFreeGate(freeGateFG, heightFG) {
		if(freeGateFG == 0 || heightFG == 0) {
			var zapFGSumm = 0;	
		}
		if(freeGateFG > 0) {
			var zapFG = $('#product-filling_out_freestanding_gate').val();
			if(zapFG == 3) {
				if(heightFG >= d1Heightd1 && heightFG <= d1Heightd2) {
					var zapFGSumm = 4500;	
				}
				if(heightFG >= d2Heightd1 && heightFG <= d2Heightd2) {
					var zapFGSumm = 4500+4500/100*10;	
				}
			}
			if(zapFG == 2) {
				if(heightFG >= d1Heightd1 && heightFG <= d1Heightd2) {
					var zapFGSumm = 6000;	
				}
				if(heightFG >= d2Heightd1 && heightFG <= d2Heightd2) {
					var zapFGSumm = 6000+6000/100*10;	
				}
			}
			if(zapFG == 1) {
				if(heightFG >= d1Heightd1 && heightFG <= d1Heightd2) {
					var zapFGSumm = 5500;	
				}
				if(heightFG >= d2Heightd1 && heightFG <= d2Heightd2) {
					var zapFGSumm = 5500+5500/100*10;	
				}
			}
		}
		return zapFGSumm;
	}
	function ZDsetka(height, width, beam, tail, tailP, freeGate) {
		var resZap = 0;
		var zapV = 0;
		var zapFGSumm = 0;
		if(beam == 1) {
			if(tail == 1) {
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w32d1 && width <= w32d2) {
					var zapV = 29200;
				}
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w352d1 && width <= w352d2) {
					var zapV = 30700;
				}
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w42d1 && width <= w42d2) {
					var zapV = 34900;
				}
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w452d1 && width <= w452d2) {
					var zapV = 36400;
				}
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w52d1 && width <= w52d2) {
					var zapV = 38200;
				}
				/*----------*/
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w32d1 && width <= w32d2) {
					var zapV = 29200+29200/100*10;
				}
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w352d1 && width <= w352d2) {
					var zapV = 30700+30700/100*10;
				}
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w42d1 && width <= w42d2) {
					var zapV = 34900+34900/100*10;
				}
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w452d1 && width <= w452d2) {
					var zapV = 36400+36400/100*10;
				}
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w52d1 && width <= w52d2) {
					var zapV = 38200+38200/100*10;
				}
			}
			if(tail == 2) {
				if(tailP == 1) {
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w32d1 && width <= w32d2) {
						var zapV = 30100;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w352d1 && width <= w352d2) {
						var zapV = 31600;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 35900;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 37400;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 39200;
					}
					/*----------*/
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w32d1 && width <= w32d2) {
						var zapV = 30100+30100/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w352d1 && width <= w352d2) {
						var zapV = 31600+31600/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 35900+35900/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 37400+37400/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 39200+39200/100*10;
					}
				}
				if(tailP == 2) {
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w32d1 && width <= w32d2) {
						var zapV = 31700;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w352d1 && width <= w352d2) {
						var zapV = 33200;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 37500;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 39000;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 40800;
					}
					/*----------*/
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w32d1 && width <= w32d2) {
						var zapV = 31700+31700/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w352d1 && width <= w352d2) {
						var zapV = 33200+33200/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 37500+37500/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 39000+39000/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 40800+40800/100*10;
					}
				}
			}
		}
		if(beam == 2) {
			if(tail == 1) {
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w42d1 && width <= w42d2) {
					var zapV = 42300;
				}
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w452d1 && width <= w452d2) {
					var zapV = 43800;
				}
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w52d1 && width <= w52d2) {
					var zapV = 45800;
				}
				/*----------*/
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w42d1 && width <= w42d2) {
					var zapV = 42300+42300/100*10;
				}
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w452d1 && width <= w452d2) {
					var zapV = 43800+43800/100*10;
				}
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w52d1 && width <= w52d2) {
					var zapV = 45800+45800/100*10;
				}
			}
			if(tail == 2) {
				if(tailP == 1) {
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 43300;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 44800;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 46800;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w552d1 && width <= w552d2) {
						var zapV = 47900;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w62d1 && width <= w62d2) {
						var zapV = 56400;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w652d1 && width <= w652d2) {
						var zapV = 57600;
					}
					/*----------*/
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 43300+43300/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 44800+44800/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 46800+46800/100*10;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w552d1 && width <= w552d2) {
						var zapV = 47900+47900/100*10;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w62d1 && width <= w62d2) {
						var zapV = 56400+56400/100*10;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w652d1 && width <= w652d2) {
						var zapV = 57600+57600/100*10;
					}
				}
				if(tailP == 2) {
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 44900;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 46400;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 47800;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w552d1 && width <= w552d2) {
						var zapV = 49000;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w62d1 && width <= w62d2) {
						var zapV = 57500;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w652d1 && width <= w652d2) {
						var zapV = 58700;
					}
					/*----------*/
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 44900+44900/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 46400+46400/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 47800+47800/100*10;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w552d1 && width <= w552d2) {
						var zapV = 49000+49000/100*10;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w62d1 && width <= w62d2) {
						var zapV = 57500+57500/100*10;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w652d1 && width <= w652d2) {
						var zapV = 58700+58700/100*10;
					}
				}
			}
		}
		resZap = parseInt(zapV)+parseInt(zapFGSumm);
		
		return resZap;
	}
	
	function reshetka(height, width, beam, tail, tailP, freeGate) {
		var resZap = 0;
		var zapV = 0;
		var zapFGSumm = 0;
		if(beam == 1) {
			if(tail == 1) {
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w32d1 && width <= w32d2) {
					var zapV = 30000;
				}
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w352d1 && width <= w352d2) {
					var zapV = 31800;
				}
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w42d1 && width <= w42d2) {
					var zapV = 35500;
				}
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w452d1 && width <= w452d2) {
					var zapV = 37600;
				}
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w52d1 && width <= w52d2) {
					var zapV = 39500;
				}
				/*----------*/
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w32d1 && width <= w32d2) {
					var zapV = 30000+30000/100*10;
				}
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w352d1 && width <= w352d2) {
					var zapV = 31800+31800/100*10;
				}
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w42d1 && width <= w42d2) {
					var zapV = 35500+35500/100*10;
				}
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w452d1 && width <= w452d2) {
					var zapV = 37600+37600/100*10;
				}
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w52d1 && width <= w52d2) {
					var zapV = 39500+39500/100*10;
				}
			}
			if(tail == 2) {
				if(tailP == 1) {
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w32d1 && width <= w32d2) {
						var zapV = 30900;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w352d1 && width <= w352d2) {
						var zapV = 32700;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 36500;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 38600;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 40500;
					}
					/*----------*/
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w32d1 && width <= w32d2) {
						var zapV = 30900+30900/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w352d1 && width <= w352d2) {
						var zapV = 32700+32700/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 36500+36500/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 38600+38600/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 40500+40500/100*10;
					}
				}
				if(tailP == 2) {
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w32d1 && width <= w32d2) {
						var zapV = 32500;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w352d1 && width <= w352d2) {
						var zapV = 34300;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 38100;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 40200;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 42100;
					}
					/*----------*/
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w32d1 && width <= w32d2) {
						var zapV = 32500+32500/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w352d1 && width <= w352d2) {
						var zapV = 34300+34300/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 38100+38100/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 40200+40200/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 42100+42100/100*10;
					}
				}
			}
		}
		if(beam == 2) {
			if(tail == 1) {
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w42d1 && width <= w42d2) {
					var zapV = 42900;
				}
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w452d1 && width <= w452d2) {
					var zapV = 45000;
				}
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w52d1 && width <= w52d2) {
					var zapV = 47100;
				}
				/*----------*/
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w42d1 && width <= w42d2) {
					var zapV = 42900+42900/100*10;
				}
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w452d1 && width <= w452d2) {
					var zapV = 45000+45000/100*10;
				}
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w52d1 && width <= w52d2) {
					var zapV = 47100+47100/100*10;
				}
			}
			if(tail == 2) {
				if(tailP == 1) {
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 43900;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 46000;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 48100;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w552d1 && width <= w552d2) {
						var zapV = 49000;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w62d1 && width <= w62d2) {
						var zapV = 59500;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w652d1 && width <= w652d2) {
						var zapV = 60500;
					}
					/*----------*/
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 43900+43900/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 46000+46000/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 48100+48100/100*10;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w552d1 && width <= w552d2) {
						var zapV = 49000+49000/100*10;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w62d1 && width <= w62d2) {
						var zapV = 59500+59500/100*10;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w652d1 && width <= w652d2) {
						var zapV = 60500+60500/100*10;
					}
				}
				if(tailP == 2) {
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 45500;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 47600;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 49400;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w552d1 && width <= w552d2) {
						var zapV = 50300;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w62d1 && width <= w62d2) {
						var zapV = 60800;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w652d1 && width <= w652d2) {
						var zapV = 61800;
					}
					/*----------*/
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 45500+45500/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 47600+47600/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 49400+49400/100*10;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w552d1 && width <= w552d2) {
						var zapV = 50300+50300/100*10;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w62d1 && width <= w62d2) {
						var zapV = 60800+60800/100*10;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w652d1 && width <= w652d2) {
						var zapV = 61800+61800/100*10;
					}
				}
			}
		}
		resZap = parseInt(zapV)+parseInt(zapFGSumm);
		return resZap;
	}
	
	function proflist(height, width, beam, tail, tailP, freeGate) {
		var resZap = 0;
		var zapV = 0;
		var zapFGSumm = 0;
		if(beam == 1) {
			if(tail == 1) {
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w32d1 && width <= w32d2) {
					var zapV = 25200;
				}
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w352d1 && width <= w352d2) {
					var zapV = 26500;
				}
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w42d1 && width <= w42d2) {
					var zapV = 29400;
				}
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w452d1 && width <= w452d2) {
					var zapV = 30800;
				}
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w52d1 && width <= w52d2) {
					var zapV = 32600;
				}
				/*----------*/
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w32d1 && width <= w32d2) {
					var zapV = 25200+25200/100*10;
				}
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w352d1 && width <= w352d2) {
					var zapV = 26500+26500/100*10;
				}
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w42d1 && width <= w42d2) {
					var zapV = 29400+29400/100*10;
				}
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w452d1 && width <= w452d2) {
					var zapV = 30800+30800/100*10;
				}
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w52d1 && width <= w52d2) {
					var zapV = 32600+32600/100*10;
				}
			}
			if(tail == 2) {
				if(tailP == 1) {
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w32d1 && width <= w32d2) {
						var zapV = 26100;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w352d1 && width <= w352d2) {
						var zapV = 27400;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 30400;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 31800;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 33600;
					}
					/*----------*/
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w32d1 && width <= w32d2) {
						var zapV = 26100+26100/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w352d1 && width <= w352d2) {
						var zapV = 27400+27400/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 30400+30400/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 31800+31800/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 33600+33600/100*10;
					}
				}
				if(tailP == 2) {
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w32d1 && width <= w32d2) {
						var zapV = 27700;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w352d1 && width <= w352d2) {
						var zapV = 29000;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 32000;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 33400;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 35200;
					}
					/*----------*/
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w32d1 && width <= w32d2) {
						var zapV = 27700+27700/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w352d1 && width <= w352d2) {
						var zapV = 29000+29000/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 32000+32000/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 33400+33400/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 35200+35200/100*10;
					}
				}
			}
		}
		if(beam == 2) {
			if(tail == 1) {
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w42d1 && width <= w42d2) {
					var zapV = 36800;
				}
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w452d1 && width <= w452d2) {
					var zapV = 38200;
				}
				if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w52d1 && width <= w52d2) {
					var zapV = 40000;
				}
				/*----------*/
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w42d1 && width <= w42d2) {
					var zapV = 36800+36800/100*10;
				}
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w452d1 && width <= w452d2) {
					var zapV = 38200+38200/100*10;
				}
				if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w52d1 && width <= w52d2) {
					var zapV = 40000+40000/100*10;
				}
			}
			if(tail == 2) {
				if(tailP == 1) {
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 37800;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 39200;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 41000;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w552d1 && width <= w552d2) {
						var zapV = 41600;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w62d1 && width <= w62d2) {
						var zapV = 48300;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w652d1 && width <= w652d2) {
						var zapV = 48800;
					}
					/*----------*/
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 37800+37800/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 39200+39200/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 41000+41000/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w552d1 && width <= w552d2) {
						var zapV = 41600+41600/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w62d1 && width <= w62d2) {
						var zapV = 48300+48300/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w652d1 && width <= w652d2) {
						var zapV = 48800+48800/100*10;
					}
				}
				if(tailP == 2) {
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 39400;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 40800;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 42100;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w552d1 && width <= w552d2) {
						var zapV = 42700;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w62d1 && width <= w62d2) {
						var zapV = 49400;
					}
					if(height >= d1Heightd1 && height <= d1Heightd2 && width >= w652d1 && width <= w652d2) {
						var zapV = 49900;
					}
					/*----------*/
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w42d1 && width <= w42d2) {
						var zapV = 39400+39400/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w452d1 && width <= w452d2) {
						var zapV = 40800+40800/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w52d1 && width <= w52d2) {
						var zapV = 42100+42100/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w552d1 && width <= w552d2) {
						var zapV = 42700+42700/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w62d1 && width <= w62d2) {
						var zapV = 49400+49400/100*10;
					}
					if(height >= d2Heightd1 && height <= d2Heightd2 && width >= w652d1 && width <= w652d2) {
						var zapV = 49900+49900/100*10;
					}
				}
			}
		}
		resZap = parseInt(zapV)+parseInt(zapFGSumm);
		return resZap;
	}
	
	setInterval(function() {
		curValProflist();
	}, 1000);
	
	/*end Под профлист*/
});
</script>
<div class="product-form">
	<?php
		$cameD = 0;
		$alutehD = 0;
		$reikaD = 0;
		$otherD = 0;
		if(!empty($discount['discount_came'])) {
			$cameD = (int) $discount['discount_came']; 
		}
		if(!empty($discount['discount_aluteh'])) {
			$alutehD = (int) $discount['discount_aluteh']; 
		}
		if(!empty($discount['discount_reika'])) {
			$reikaD = (int) $discount['discount_reika']; 
		}
		if(!empty($discount['discount_other'])) {
			$otherD = (int) $discount['discount_other']; 
		}
	?>
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="hid_disc" style="display: none;">
			<input class="cameD" type="number" readonly name="cameD" value="<?php echo $cameD; ?>">
			<input class="alutehD" type="number" readonly name="alutehD" value="<?php echo $alutehD; ?>">
			<input class="reikaD" type="number" readonly name="reikaD" value="<?php echo $reikaD; ?>">
			<input class="otherD" type="number" readonly name="otherD" value="<?php echo $otherD; ?>">
		</div>
        <div class="col-md-8" style="width: 100%;">
			<h4>Ворота</h4>
			<div class="row" style="display: none;">
				<div class="col-md-6">
					<?= $form->field($model, 'arr_prices')->textArea() ?>
				</div>		
			</div>
			<hr class="hor-line">
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'name')->textInput() ?>
                </div>
				<div class="col-md-3">
					<?= $form->field($model, 'rollback_side')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'влево',
							2 => 'вправо',
						] );
					?>
				</div>
                <div class="col-md-3">
                    <?= $form->field($model, 'height')->input('number') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'width')->input('number') ?>
                </div>
            </div>
			<div class="row">
				<div class="col-md-3">
					<?= $form->field($model, 'locking_device')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'нет',
							2 => 'да',
						] );
					?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'dyeing')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'гладкая',
							2 => 'шагрень',
						] );
					?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'color')->dropdownList(
						[0 =>'Выберите вариант',
							3005 => '3005',
							5005 => '5005',
						 	6005 => '6005',
						 	7004 => '7004',
							7024 => '7024',
						 	7035 => '7035',
						 	7040 => '7040',
							8017 => '8017',
						 	8019 => '8019',
						 	9003 => '9003',
						 	9005 => '9005',
						] );
					?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'beam')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'Эко SGN 01',
							2 => 'Евро SGN 02',
						] );
					?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<?= $form->field($model, 'tail')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'косой',
							2 => 'прямой',
						] );
					?>
				</div>
				<div class="col-md-4">
					<?= $form->field($model, 'tail_pr')->dropdownList(
						[0 =>'Выберите вариант',
							//1 => 'два п-образных столба',
							2 => 'три п-образных столба',
						] );
					?>
				</div>
			</div>
			<h4>Рейка зубчатая</h4>
			<hr class="hor-line">
			<div class="row">
				<div class="col-md-6">
					<?= $form->field($model, 'toothed_rack')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'Авторасчет',
							2 => 'Задать количество',
						] );
					?>
				</div>
				<div class="col-md-6">
					<?= $form->field($model, 'toothed_rack_count')->input('number') ?>
				</div>
			</div>
			<hr class="hor-line">
			<h4>Заполнение</h4>
			<hr class="hor-line">
			<div class="row">
				<div class="col-md-3">
					<?= $form->field($model, 'filling_out')->dropdownList(
						[	3 => 'Под профлист',
							1 => 'Заполнение 3Д сетка',
						] );
					?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'staff_with_professional_sheet')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'нет',
							2 => 'да',
						] );
					?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'profiled_sheet_S8045mm')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'Профлист С8 0,45мм цвет с одной стороны',
							2 => 'Профлист С8 0,45мм цвет с двух сторон',
						] );
					?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'compl_proflist')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'Заполнение с одной стороны',
							2 => 'Заполнение с двух сторон',
						] );
					?>
				</div>
			</div>
			<hr class="hor-line">
			<h4>Калитка встроеная</h4>
			<hr class="hor-line">
			<div class="row row-b-gate">
				<div class="col-md-3">
					<?= $form->field($model, 'builtin_gate')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'нет',
							2 => 'да',
						] );
					?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'gate_location_w')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'по центру',
							2 => 'слева',
						 	3 => 'справа',
						] );
					?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'gate_opening_w')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'на улицу',
							2 => 'во двор',
						] );
					?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'wicket_lock')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'нет',
							2 => 'да',
						] );
					?>
				</div>
				<div class="col-md-4">
					<?= $form->field($model, 'tail_distance')->input('number') ?>
				</div>
			</div>
			<hr class="hor-line">
			<h4>Калитка отдельностоящая</h4>
			<hr class="hor-line">
			<div class="row row-out-gate">
				<div class="col-md-3">
					<?= $form->field($model, 'type_freestanding_gate')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'в раме',
							2 => 'со столбами',
						 	3 => 'без столбов и рамы',
						] );
					?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'hinges')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'Слева',
							2 => 'Справа',
						] );
					?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'opening_side')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'На улицу',
							2 => 'Во двор',
						] );
					?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'lock_gate')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'нет',
							2 => 'да',
						] );
					?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'filling_out_freestanding_gate')->dropdownList(
						[	3 => 'Под профлист',
							1 => 'Заполнение 3Д сетка',						 	
						] );
					?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'staff_with_professional_sheet_ko')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'нет',
							2 => 'да',
						] );
					?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'profiled_sheet_S8045mm_ko')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'Профлист С8 0,45мм цвет с одной стороны',
							2 => 'Профлист С8 0,45мм цвет с двух сторон',
						] );
					?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'compl_proflist_ko')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'Заполнение с одной стороны',
							2 => 'Заполнение с двух сторон',
						] );
					?>
				</div>
			</div>
			<div class="in-frame-choose">
				<div class="row">
					<div class="col-md-3">
						<?= $form->field($model, 'frame_width')->input('number', ['step'=> '5']) ?>
					</div>
					<div class="col-md-3">
						<?= $form->field($model, 'frame_height')->input('number', ['step'=> '5']) ?>
					</div>
					<div class="col-md-3">
						<?= $form->field($model, 'frame_profile')->dropdownList(
							[0 =>'Выберите вариант',
								1 => 'рама 60х30',
								2 => 'рама 60х60',
								3 => 'рама 80х80/80х40',
							] );
						?>
					</div>
				</div>
				<hr class="hor-line">
			</div>
			<div class="on-column-choose">
				<div class="row">
					<div class="col-md-3">
						<?= $form->field($model, 'gate_columns')->input('number', ['step'=> '5']) ?>
					</div>
					<div class="col-md-3">
						<?= $form->field($model, 'gate_height')->input('number', ['step'=> '5']) ?>
					</div>
					<div class="col-md-3">
						<?= $form->field($model, 'columns_r')->dropdownList(
							[0 =>'Выберите вариант',
								1 => '60х30',
								2 => '60х60',
							 	3 => '80х80',
							] );
						?>
					</div>
					<div class="col-md-3">
						<?= $form->field($model, 'height60x60')->dropdownList(
							[0 =>'Выберите вариант',
								1 => 'по калитке +100мм',
								2 => '3000мм',
							] );
						?>
					</div>
					<div class="col-md-3">
						<?= $form->field($model, 'height80x80')->dropdownList(
							[0 =>'Выберите вариант',
								1 => 'по калитке +100мм',
								2 => '3000мм',
							] );
						?>
					</div>
				</div>
				<hr class="hor-line">
			</div>
			<div class="out-column-frame-choose">
				<div class="row">
					<div class="col-md-6">
						<?= $form->field($model, 'height_without_cf')->input('number', ['step'=> '5']) ?>
					</div>
					<div class="col-md-6">
						<?= $form->field($model, 'width_without_cf')->input('number', ['step'=> '5']) ?>
					</div>
				</div>
				<hr class="hor-line">
			</div>
			<h4>Закладные</h4>
			<hr class="hor-line">
			<div class="row">
				<div class="col-md-6">
					<?= $form->field($model, 'channel_P12')->input('number') ?>
				</div>
				<div class="col-md-6">
					<?= $form->field($model, 'concrete_mortgage')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'нет',
							2 => 'да',
						] );
					?>
				</div>
			</div>
			<hr class="hor-line">
			<h4>Винтовые сваи</h4>
			<hr class="hor-line">
			<div class="row">
				<div class="col-md-3">
					<?= $form->field($model, 'pile_57x2000mm')->input('number') ?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'heading_57')->input('number') ?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'pile_76x2000mm')->input('number') ?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'heading_76')->input('number') ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<?= $form->field($model, 'pile_89x2000mm')->input('number') ?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'heading_89')->input('number') ?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'pile_108x2000mm')->input('number') ?>
				</div>
				<div class="col-md-3">
					<?= $form->field($model, 'heading_108')->input('number') ?>
				</div>
			</div>
			<hr class="hor-line">
			<h4>Автоматика</h4>
			<hr class="hor-line">
			<div class="row">
				<div class="col-md-4">
					<?= $form->field($model, 'automation')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'Алютех',
							2 => 'САМЕ',
						] );
					?>
				</div>
				<div class="col-md-4">
					<?= $form->field($model, 'lamp')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'нет',
							2 => 'да',
						] );
					?>
				</div>
				<div class="col-md-4">
					<?= $form->field($model, 'dop_pult')->input('number') ?>
				</div>
			</div>
			<div class="row row-combo">
				<div class="col-md-6">
					<?= $form->field($model, 'combo_kit')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'Combo BX608',
							2 => 'Combo BX708',
						] );
					?>
				</div>
				<div class="col-md-6">
					<?= $form->field($model, 'antenna')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'нет',
							2 => 'да',
						] );
					?>
				</div>
			</div>
			<div class="row row-aluteh">
				<div class="col-md-6">
					<?= $form->field($model, 'photocells')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'нет',
							2 => 'да',
						] );
					?>
				</div>
				<div class="col-md-6">
					<?= $form->field($model, 'drive_unit')->dropdownList(
						[0 =>'Выберите вариант',
							1 => 'RTO500KIT',
							2 => 'RTO1000KIT',
						 	3 => 'RTO2000KIT',
						] );
					?>
				</div>
			</div>
			<hr class="hor-line">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group  has-success">
                        <label class="control-label" for="product-price">Сумма</label>
                        <input type="number" id="productSum" class="form-control" aria-invalid="false" disabled="disabled">
                        <div class="help-block"></div>
						<div class="form-group  has-success" style="display: none;">
							<?= $form->field($model, 'price')->input('number') ?>
							<div class="help-block"></div>
						</div>
                    </div>
                </div>
            </div>
			<hr class="hor-line">
			<div class="row">
				<img src="/img/1l1.jpg" style="width: 100%; height: auto;">	
			</div>	
        </div>
    </div>




    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
</div>



<?php

/*if($model->category != null){
    $paramCategory = Param::find()->where(['id' => $model->category])->one();
    $paramCategoryPrice = $paramCategory->price;
    $paramCategorySizeCounting = json_encode($paramCategory->size_counting == 1 ? true : false);
} else {
    $paramCategoryPrice = 0;
    $paramCategorySizeCounting = json_encode(false);
}

if($model->direction != null){
    $paramDirection = Param::find()->where(['id' => $model->direction])->one();
    $paramDirectionPrice = $paramDirection->price;
    $paramDirectionSizeCounting = json_encode($paramDirection->size_counting == 1 ? true : false);
} else {
    $paramDirectionPrice = 0;
    $paramDirectionSizeCounting = json_encode(false);
}

if($model->metal != null){
    $paramMetal = Param::find()->where(['id' => $model->metal])->one();
    $paramMetalPrice = $paramMetal->price;
    $paramMetalSizeCounting = json_encode($paramMetal->size_counting == 1 ? true : false);
} else {
    $paramMetalPrice = 0;
    $paramMetalSizeCounting = json_encode(false);
}

if($model->castle != null){
    $paramCastle = Param::find()->where(['id' => $model->castle])->one();
    $paramCastlePrice = $paramCastle->price;
    $paramCastleSizeCounting = json_encode($paramCastle->size_counting == 1 ? true : false);
} else {
    $paramCastlePrice = 0;
    $paramCastleSizeCounting = json_encode(false);
}

if($model->transom != null){
    $paramTransom = Param::find()->where(['id' => $model->transom])->one();
    $paramTransomPrice = $paramTransom->price;
    $paramTransomSizeCounting = json_encode($paramTransom->size_counting == 1 ? true : false);
} else {
    $paramTransomPrice = 0;
    $paramTransomSizeCounting = json_encode(false);
}

if($model->glass_window != null){
    $paramGlassWindow = Param::find()->where(['id' => $model->glass_window])->one();
    $paramGlassWindowPrice = $paramGlassWindow->price;
    $paramGlassWindowSizeCounting = json_encode($paramGlassWindow->size_counting == 1 ? true : false);
} else {
    $paramGlassWindowPrice = 0;
    $paramGlassWindowSizeCounting = json_encode(false);
}

if($model->chipper != null){
    $paramChipper = Param::find()->where(['id' => $model->chipper])->one();
    $paramChipperPrice = $paramChipper->price;
    $paramChipperSizeCounting = json_encode($paramChipper->size_counting == 1 ? true : false);
} else {
    $paramChipperPrice = 0;
    $paramChipperSizeCounting = json_encode(false);
}

if($model->closer != null){
    $paramCloser = Param::find()->where(['id' => $model->closer])->one();
    $paramCloserPrice = $paramCloser->price;
    $paramCloserSizeCounting = json_encode($paramCloser->size_counting == 1 ? true : false);
} else {
    $paramCloserPrice = 0;
    $paramCloserSizeCounting = json_encode(false);
}

if($model->handle != null){
    $paramHandle = Param::find()->where(['id' => $model->handle])->one();
    $paramHandlePrice = $paramHandle->price;
    $paramHandleSizeCounting = json_encode($paramHandle->size_counting == 1 ? true : false);
} else {
    $paramHandlePrice = 0;
    $paramHandleSizeCounting = json_encode(false);
}

$height = $model->height != null ? $model->height : 0;
$width = $model->width != null ? $model->width : 0;
$delivery = $model->delivery != null ? $model->delivery : 0;
$install = $model->install != null ? $model->install : 0;
$size = $height * $width;

$count = $model->count != null ? $model->count : 1;

$script = "
var prices = {
    calcAttributes: {
        ".Param::TYPE_CATEGORY.": {
            price: $paramCategoryPrice,
            sizeCounting: $paramCategorySizeCounting,
        },
        ".Param::TYPE_DIRECTION.": {
            price: $paramDirectionPrice,
            sizeCounting: $paramDirectionSizeCounting,
        },
        canvasWidth: 0,
        ral: 0,
        ".Param::TYPE_METAL.": {
            price: $paramMetalPrice,
            sizeCounting: $paramMetalSizeCounting,
        },
        ".Param::TYPE_HANDLE.": {
            price: $paramHandlePrice,
            sizeCounting: $paramHandleSizeCounting,
        },
        ".Param::TYPE_CASTLE.": {
            price: $paramCastlePrice,
            sizeCounting: $paramCastleSizeCounting,
        },
        ".Param::TYPE_TRANSOM.": {
            price: $paramTransomPrice,
            sizeCounting: $paramTransomSizeCounting,
        },
        ".Param::TYPE_GLASS_WINDOW.": {
            price: $paramGlassWindowPrice,
            sizeCounting: $paramGlassWindowSizeCounting,
        },
        ".Param::TYPE_CHIPPER.": {
            price: $paramChipperPrice,
            sizeCounting: $paramChipperSizeCounting,
        },
        ".Param::TYPE_CLOSER.": {
            price: $paramCloserPrice,
            sizeCounting: $paramCloserSizeCounting,
        },
        delivery: $delivery,
        install: $install,
    },
    sizeAttributes: {
        height: $height,
        width: $width,
        additionalWidth: 0,
        size: $size,
    },
    count: $count,
    calcSize: function(){
        this.sizeAttributes.size = this.sizeAttributes.height * this.sizeAttributes.width;    
    },
    calculate: function(){
        this.calcSize();
        
        var sum = 0;
        for(var key in this.calcAttributes){
            var self = this.calcAttributes;
            if(typeof self[key] == 'number'){
                sum += self[key];
            } else if(typeof self[key] == 'object'){
                if(self[key].sizeCounting == true){
                    sum += self[key].price * this.sizeAttributes.size;
                } else {
                    sum += self[key].price;
                }
            }
        }
        
        var price = sum;
        sum = sum * this.count;
        
        $('#product-price').val(price);
        $('#productSum').val(sum);
    }
};

var DoorDesigner = function(){
};

DoorDesigner.setCategory = function(img){
    $('#door-designer-img').attr('src', '/'+img);
};

DoorDesigner.setHandle = function(img){
    $('#door-designer-img_handle').attr('src', '/'+img);
};

DoorDesigner.setWindowGlass = function(img){
    $('#door-designer-img_window_glass').attr('src', '/'+img);
};

DoorDesigner.setTransom = function(img){
    $('#door-designer-img_transom').attr('src', '/'+img);
};

DoorDesigner.setChipper = function(img){
    $('#door-designer-img_chipper').attr('src', '/'+img);
};

$('#product-width').change(function(){
    prices.sizeAttributes.width = parseFloat($(this).val() != '' ? $(this).val() : 0);
    prices.calculate();
});

$('#product-height').change(function(){
    prices.sizeAttributes.height = parseFloat($(this).val() != '' ? $(this).val() : 0);
    prices.calculate();
});

$('#product-delivery').change(function(){
    prices.calcAttributes.delivery = parseFloat($(this).val() != '' ? $(this).val() : 0);
    prices.calculate();
});

$('#product-install').change(function(){
    prices.calcAttributes.install = parseFloat($(this).val() != '' ? $(this).val() : 0);
    prices.calculate();
});

$('#product-count').change(function(){
    prices.count = $(this).val();
    prices.calculate();
});

$('#product-price').change(function(){
    $('#productSum').val($('#product-price').val()*$('#product-count').val());
});

$('#product-category, #product-subcategory, #product-direction, #product-handle, #product-metal, #product-castle, #product-transom, #product-glass_window, #product-chipper, #product-closer').change(function(){
    var value = $(this).val();
    var ziro = false;
    
    if(value == ''){
        value = $(this).find('option:last-child').attr('value');
        ziro = true;
    }
    
    var id = $(this).attr('id');
    
    $.get('/param/view-ajax?id='+value, function(response){
        var price = response.price;
        var sizeCounting = response.size_counting == 1 ? true : false;
    
        if(id == 'product-category'){
            DoorDesigner.setCategory(response.img_url);
        }
        if(id == 'product-handle'){
            DoorDesigner.setHandle(response.img_url);
        }
        if(id == 'product-glass_window'){
            DoorDesigner.setWindowGlass(response.img_url);
        }
        if(id == 'product-chipper'){
            DoorDesigner.setChipper(response.img_url);
        }
        if(id == 'product-transom'){
            DoorDesigner.setTransom(response.img_url);
        }
        
        if(ziro == false){
            prices.calcAttributes[response.type] = {'price': price, 'sizeCounting': sizeCounting};
        } else {
            prices.calcAttributes[response.type] = {'price': 0, 'sizeCounting': sizeCounting};
        }
        prices.calculate();
    });
});


";

$this->registerJs($script, \yii\web\View::POS_READY);*/


?>

