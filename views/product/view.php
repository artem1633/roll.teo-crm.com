<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
?>
<div class="product-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            'user_id',
            'category',
            'direction',
            'height',
            'width',
            'additional_width',
            'canvas_width',
            'ral',
            'metal',
            'castle',
            'handle',
            'glass_window',
            'chipper',
            'closer',
            'transom',
            'delivery',
            'install',
            'created_at',
        ],
    ]) ?>

</div>
