<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'specification',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'specification_path',
        'label' => 'Файл спецификации',
        'content' => function($model) {
            return Html::a('Спецификация', '/'.$model->specification_path, ['download' => 'Спецификация', 'data-pjax' => '0']);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'bill',
        'content' => function($model) {
            return Html::a('Счет', '/'.$model->specification_path, ['download' => 'Счет', 'data-pjax' => '0']);
        },
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'kp',
//        'content' => function($model) {
//            return Html::a('КП', '/'.$model->kp, ['download' => 'КП', 'data-pjax' => '0']);
//        },
//    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'start',
         'content' => function($model) {
             return Html::a('Запуск', '/'.$model->start, ['download' => 'Запуск', 'data-pjax' => '0']);
         },
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'payment_date',
         'format' => ['date', 'php:d M Y'],
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'start_date',
         'format' => ['date', 'php:d M Y'],
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'reload_date',
         'format' => ['date', 'php:d M Y'],
     ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{update}{delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                if(Yii::$app->user->identity->isProduction()){
                    return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                        'role'=>'modal-remote', 'title'=>'Удалить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-confirm-title'=>'Вы уверены?',
                        'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                    ]);
                }
            },
            'update' => function ($url, $model) {
                if(Yii::$app->user->identity->isProduction()){
                    return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                            'role'=>'modal-remote', 'title'=>'Изменить',
                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                            'data-request-method'=>'post',
                        ])."&nbsp;";
                }
            }
        ],
    ],

];   