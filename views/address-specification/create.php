<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AddressSpecification */

?>
<div class="address-specification-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
