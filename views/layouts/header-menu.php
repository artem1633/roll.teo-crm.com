<?php

use yii\helpers\Url;

?>

<div id="top-menu" class="top-menu">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\TopMenu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Заказы', 'icon' => 'fa  fa-rub', 'url' => ['/order'],],
//                    ['label' => 'Двери', 'icon' => 'fa  fa-folder-open', 'url' => ['/product'],],
//                    ['label' => 'Параметры', 'icon' => 'fa  fa-cog', 'url' => ['/param'],],
                    ['label' => 'Пользователи', 'icon' => 'fa  fa-user-o', 'url' => ['/user'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                    ['label' => 'Производство', 'icon' => 'fa  fa-cubes', 'url' => ['/company'],],
                    ['label' => 'Справочники', 'icon' => 'fa  fa-book', 'url' => '#',  'visible' => Yii::$app->user->identity->isSuperAdmin(), 'options' => ['class' => 'has-sub'], 'items' => [
                        ['label' => 'Примечания', 'url' => ['/comment'],],
                        ['label' => 'Статусы заказов', 'url' => ['/order-status'],],
                    ]],
					['label' => 'Управление чертежами', 'icon' => 'fa  fa-book', 'url' => ['/admin/update-img'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
