<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Company */
?>
<div class="company-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'email:email',
            'phone',
        ],
    ]) ?>

</div>
