<?php

/** @var $model \app\models\Order */

use yii\widgets\DetailView;
use yii\helpers\Url;

$this->title = "Заказ №{$model->id} «{$model->name}»";

?>
<br>
<h4 style="font-family: Calibri; margin-top: 60px; text-align: left; font-size: 12px;">ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "РОЛПОСТ"</h4>
<h4 style="font-family: Calibri; margin-top: 20px; text-align: left; font-size: 12px;">187032, Ленинградская обл, Тосненский р-н, поселок Тельмана, ул Красноборская дорога, д 3, пом 15, тел. +79217665081</h4>
<h4 style="font-family: Calibri; margin-top: 20px; text-align: center; font-size: 12px;"><?="Заказ №{$model->id} от ".Yii::$app->formatter->asDatetime($model->created_at, 'php:d.m.Y')?></h4>

<div class="content">
    <?php $totalSum = 0; $counter = 0; foreach ($model->products as $product): ?>
    <?php
    $productCommentPks = \yii\helpers\ArrayHelper::getColumn(\app\models\ProductComment::find()->where(['product_id' => $product->id])->all(), 'comment_id');
    $comments = \app\models\Comment::find()->where(['id' => $productCommentPks])->all();
	
    ?>
	<?php $prices = (array) json_decode($product->arr_prices, true); ?>
	
	<h3 style="font-family: Calibri; width: 1000px; font-weight: bold; text-align: left; font-size: 12px; padding-left: 7px;">Коммерческое предложение</h3>
    <table style="width: 100%;  border: 1px solid #000; padding-top: -9px;" class="pdf-kp">
		
       	<tr style="border: 1px solid #000;"><td style="border: none;"></td></tr>
		
		<tr>
            <td style="width: 60%; text-align: center; font-size: 10px;">Наименование</td>
			<td style="width: 15%; text-align: center; font-size: 10px;">Цена</td>
			<td style="width: 5%; text-align: center; font-size: 10px;">Кол-во</td>
			<td style="width: 5%; text-align: center; font-size: 10px;">Ед. изм.</td>
			<td style="width: 15%; text-align: center; font-size: 10px;">Сумма</td>
		</tr>
		<?php if($prices['zapRes'] > 0) { ?>
			<tr>
				<td style="text-align: left; "><?php $roll = 'Откатные ворота '.$product->width.'x'.$product->height;
					if($product->beam == 1) $roll .= ' Эко SGN 01';
					if($product->beam == 2) $roll .= ' Евро SGN 02';
					if($product->filling_out == 3) $roll .= ' под зашивку профлистом';
					if($product->filling_out == 1) $roll .= ' ЗД сетка';
					if($product->filling_out == 2) $roll .= ' ЗД "решетка"';
					echo $roll;
					?>
				</td>
				<td><?php echo $prices['zapRes'];?></td><td>1</td><td>шт</td><td><?php echo $prices['zapRes'];?></td>
			</tr>
		<?php } ?>
		<?php if($prices['zapUstr'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Запорное устройство</td><td><?php echo $prices['zapUstr'];?></td>
				<td>1</td><td>шт</td><td><?php echo $prices['zapUstr'];?></td>
			</tr>
		<?php } ?>
		<?php if($prices['resRack'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Рейка зубчатая</td><td><?php echo $prices['resRack']/$product->toothed_rack_count;?></td>
				<td><?php echo $product->toothed_rack_count;?></td><td>шт</td><td><?php echo $prices['resRack'];?></td>
			</tr>
		<?php } ?>
		<?php if($prices['summ'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Калитка встроенная</td><td><?php echo $prices['summ'];?></td>
				<td>1</td><td>шт</td><td><?php echo $prices['summ'];?></td>
			</tr>
		<?php } ?>
		<?php if($prices['zamSum'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Замок врезной Stublina</td><td><?php echo $prices['zamSum'];?></td>
				<td>1</td><td>шт</td><td><?php echo $prices['zamSum'];?></td>
			</tr>
		<?php } ?>
		<?php if($prices['bZ'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Закладная на ворота 4м с арматурой для монтажа на бетон</td><td><?php echo $prices['bZ'];?></td>
				<td>1</td><td>шт</td><td><?php echo $prices['bZ'];?></td>
			</tr>
		<?php } ?>
		<?php if($prices['shV'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Швеллер П12</td><td>850</td>
				<td><?php echo $product->channel_P12?></td><td>м</td><td><?php echo $prices['shV'];?></td>
			</tr>
		<?php } ?>
		<?php if($prices['summFreeGateF'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Калитка отдельностоящая</td><td><?php echo $prices['summFreeGateF']; ?></td>
				<td>1</td><td>шт</td><td><?php echo $prices['summFreeGateF'];?></td>
			</tr>
			<tr>
				<td style="text-align: left; ">
					<?php if($product->type_freestanding_gate == 1) {
						$rama = 'Рама калитки из профиля ';
						if($product->frame_profile == 1) $rama .= '60x30';
						if($product->frame_profile == 2) $rama .= '60x60';
						if($product->frame_profile == 3) $rama .= '80х80/80х40';
						echo $rama;
					}?>
					<?php if($product->type_freestanding_gate == 2) {
						$rama = 'Со столбами ';
						if($product->columns_r == 1) $rama .= '60x30';
						if($product->columns_r == 2) $rama .= '60x60';
						if($product->columns_r == 3) $rama .= '80х80';
						echo $rama;
					}?>
					<?php if($product->type_freestanding_gate == 3) {
						$rama = 'Без столбов и рамы ';
						echo $rama;
					}?>
				</td>
				<td><?php echo $prices['summFreeGateF']; ?></td>
				<td>1</td><td>шт</td><td><?php echo $prices['summFreeGateF'];?></td>
			</tr>
		<?php } ?>
		<?php if($prices['zamSumV'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Замок врезной Stublina</td><td><?php echo $prices['zamSumV'];?></td>
				<td>1</td><td>шт</td><td><?php echo $prices['zamSumV'];?></td>
			</tr>
		<?php } ?>
		<?php if($prices['sumPF'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">
					<?php 
						$pf = '';
						if($product->profiled_sheet_S8045mm == 1) $pf .= 'Профлист С8 0,45мм цвет с одной стороны';
						if($product->profiled_sheet_S8045mm == 2) $pf .= 'Профлист С8 0,45мм цвет с двух сторон';
						echo $pf;
					?>
				</td><td><?php echo $prices['koefPF'];?></td>
				<td><?php echo $prices['d']*$prices['hList']; ?></td><td>пог. м</td><td><?php echo $prices['koefPF']*$prices['d']*$prices['hList'];?></td>
			</tr>
		<?php } ?>
		<?php if($prices['packSheet'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Упаковка профлиста</td><td>250</td>
				<td><?php echo $prices['packSheet']/250;?></td><td>пог. м</td><td><?php echo $prices['packSheet'];?></td>
			</tr>
		<?php } ?>
		<?php if($prices['samrez'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Кровельные саморезы</td><td>2.50</td>
				<td><?php echo $prices['samrez']/2.50;?></td><td>шт</td><td><?php echo $prices['samrez'];?></td>
			</tr>
		<?php } ?>
		<?php if($product->pile_57x2000mm > 0) { ?>
			<tr>
				<td style="text-align: left; ">D57*3,5мм 2000мм</td><td>938</td>
				<td><?php echo $product->pile_57x2000mm; ?></td><td>шт</td><td><?php echo $product->pile_57x2000mm*938; ?></td>
			</tr>
		<?php } ?>
		<?php if($product->pile_76x2000mm > 0) { ?>
			<tr>
				<td style="text-align: left; ">D76*3,5мм 2000мм</td><td>1290</td>
				<td><?php echo $product->pile_76x2000mm; ?></td><td>шт</td><td><?php echo $product->pile_76x2000mm*1290; ?></td>
			</tr>
		<?php } ?>
		<?php if($product->pile_89x2000mm > 0) { ?>
			<tr>
				<td style="text-align: left; ">D89*3,5мм 2000мм</td><td>1380</td>
				<td><?php echo $product->pile_89x2000mm; ?></td><td>шт</td><td><?php echo $product->pile_89x2000mm*1380; ?></td>
			</tr>
		<?php } ?>
		<?php if($product->pile_108x2000mm > 0) { ?>
			<tr>
				<td style="text-align: left; ">D108*3,5мм 2000мм</td><td>1750</td>
				<td><?php echo $product->pile_108x2000mm; ?></td><td>шт</td><td><?php echo $product->pile_108x2000mm*1750; ?></td>
			</tr>
		<?php } ?>
		<?php if($product->heading_57 > 0) { ?>
			<tr>
				<td style="text-align: left; ">Оголовок D57</td><td>230</td>
				<td><?php echo $product->heading_57; ?></td><td>шт</td><td><?php echo $product->heading_57*230; ?></td>
			</tr>
		<?php } ?>
		<?php if($product->heading_76 > 0) { ?>
			<tr>
				<td style="text-align: left; ">Оголовок D76</td><td>250</td>
				<td><?php echo $product->heading_76; ?></td><td>шт</td><td><?php echo $product->heading_76*250; ?></td>
			</tr>
		<?php } ?>
		<?php if($product->heading_89 > 0) { ?>
			<tr>
				<td style="text-align: left; ">Оголовок D89</td><td>250</td>
				<td><?php echo $product->heading_89; ?></td><td>шт</td><td><?php echo $product->heading_89*250; ?></td>
			</tr>
		<?php } ?>
		<?php if($product->heading_108 > 0) { ?>
			<tr>
				<td style="text-align: left; ">Оголовок D108</td><td>280</td>
				<td><?php echo $product->heading_108; ?></td><td>шт</td><td><?php echo $product->heading_108*280; ?></td>
			</tr>
		<?php } ?>
		<?php if($prices['automatSummAl'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">
					<?php $autoAl = 'Комплект автоматики Алютех '; 
						if($product->drive_unit == 1) $autoAl .= 'RTO500KIT';
						if($product->drive_unit == 2) $autoAl .= 'RTO1000KIT';
						if($product->drive_unit == 3) $autoAl .= 'RTO2000KIT';
						$autoAl .= ' (привод, 4х канальные пульты 2шт, монтажная пластина)';
						echo $autoAl;
					?>
				</td><td><?php echo $prices['automatSummAl'];?></td>
				<td>1</td><td>шт</td><td><?php echo $prices['automatSummAl'];?></td>
			</tr>
		<?php } ?>
		<?php if($prices['lampAl'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Сигнальная лампа Алютех SL-U</td><td><?php echo $prices['lampAl'];  ?></td>
				<td>1</td><td>шт</td><td><?php echo $prices['lampAl']; ?></td>
			</tr>
		<?php } ?>
		<?php if($prices['photoAl'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Фотоэлементы Алютех LM-L</td><td><?php echo $prices['photoAl'];  ?></td>
				<td>1</td><td>шт</td><td><?php echo $prices['photoAl']; ?></td>
			</tr>
		<?php } ?>
		<?php if($prices['pultAl'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Брелок передатчик AT-4N</td><td>1045</td>
				<td><?php echo $prices['pultAl']/1045; ?></td><td>шт</td><td><?php echo $prices['pultAl']; ?></td>
			</tr>
		<?php } ?>
		<?php if($prices['automatSummCombo'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">
					<?php $autoCombo = 'Комплект автоматики CAME '; 
						if($product->combo_kit == 1) $autoCombo .= 'Combo BX608';
						if($product->combo_kit == 2) $autoCombo .= 'Combo BX708';
						$autoCombo .= ' (привод, 4х канальные пульты 2шт, монтажная пластина)';
						echo $autoCombo;
					?>
				</td><td><?php echo $prices['automatSummCombo'];?></td>
				<td>1</td><td>шт</td><td><?php echo $prices['automatSummCombo'];?></td>
			</tr>
		<?php } ?>
		<?php if($prices['lampCombo'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">CAME 001KLED Сигнальная лампа (светодиодная) 230В</td><td><?php echo $prices['lampCombo'];  ?></td>
				<td>1</td><td>шт</td><td><?php echo $prices['lampCombo']; ?></td>
			</tr>
		<?php } ?>
		<?php if($prices['antennaCombo'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Антенна САМЕ</td><td><?php echo $prices['antennaCombo'];  ?></td>
				<td>1</td><td>шт</td><td><?php echo $prices['antennaCombo']; ?></td>
			</tr>
		<?php } ?>
		<?php if($prices['pultCombo'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Брелок-передатчик 4-х канальный TOP44RGR</td><td>1320</td>
				<td><?php echo $prices['pultCombo']/1320; ?></td><td>шт</td><td><?php echo $prices['pultCombo']; ?></td>
			</tr>
		<?php } ?>
	
    </table>
	<h4>Общая стоимость: <?php echo $product->price;?> руб.</h4>
	<pagebreak>
	<h3 style="font-family: Calibri; margin-top: 20px; text-align: center; font-size: 12px;"><?="Техническое задание к заказу №{$model->id} от ".Yii::$app->formatter->asDatetime($model->created_at, 'php:d.m.Y')?></h3>
	<table style="width: 100%; border: 1px solid #000;">
		<tr><td style="width: 100%; text-align: center; font-weight: bold; font-size: 12px; padding-top: 10px;">Эскиз ворот с обозначениями (вид со стороны двора)</td></tr>
		<tr>
			<td>
				<img src="<?php echo $prices['imgPath'];?>" style="width: 800px; height: auto;">
			</td>
		</tr>
	</table>
	
	<table style="width: 100%;">
		<tr>
			<td>
	<table style="width: 100%;  border: 1px solid #000; padding-top: -9px; margin-top: 20px;" class="pdf-kp">
		<tr style="border: 1px solid #000;"><td style="border: none;"></td></tr>
		<tr>
            <td style="width: 70%; text-align: center; font-size: 10px;">Наименование</td>
			<td style="width: 15%; text-align: center; font-size: 10px;">Кол-во</td>
			<td style="width: 15%; text-align: center; font-size: 10px;">Ед. изм.</td>
		</tr>
		<?php if($prices['zapRes'] > 0) { ?>
			<tr>
				<td style="text-align: left; "><?php $roll = 'Откатные ворота '.$product->width.'x'.$product->height;
					if($product->filling_out == 3) $roll .= ' под зашивку профлистом';
					if($product->filling_out == 1) $roll .= ' ЗД сетка';
					if($product->filling_out == 2) $roll .= ' ЗД "решетка"';
					echo $roll;
					?>
				</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->width > 0) { ?>
			<tr>
				<td style="text-align: left; ">A
				</td>
				<td><?php echo $product->width;?></td><td>мм</td>
			</tr>
		<?php } ?>
		<?php if($product->height > 0) { ?>
			<tr>
				<td style="text-align: left; ">B
				</td>
				<td><?php echo $product->height;?></td><td>мм</td>
			</tr>
		<?php } ?>
		<?php if($product->beam > 0) { ?>
			<tr>
				<td style="text-align: left; ">
					<?php
						$beam = 'Балка';
						if($product->beam == 1) $beam .= ' Эко SGN 01';
						if($product->beam == 2) $beam .= ' Евро SGN 02';
						echo $beam;
					?>
				</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->rollback_side > 0) { ?>
			<tr>
				<td style="text-align: left; ">
					<?php
						$rollback = 'Сторона отката(вид изнутри)';
						if($product->rollback_side == 1) $rollback .= ' влево';
						if($product->rollback_side == 2) $rollback .= ' вправо';
						echo $rollback;
					?>
				</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['zapUstr'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Запорное устройство</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->dyeing > 0) { ?>
			<tr>
				<td style="text-align: left; ">
					<?php 
						$dyeing = 'Покраска';
						if($product->dyeing == 1) $dyeing .= ' гладкая';
						if($product->dyeing == 2) $dyeing .= ' шагрень';
						echo $dyeing;
					?>
				</td>
				<td><?php echo $product->color; ?></td><td>цвет</td>
			</tr>
		<?php } ?>
		<?php if($product->tail > 0) { ?>
			<tr>
				<td style="text-align: left; ">
					<?php 
						$tail = 'Хвост';
						if($product->tail == 1) $tail .= ' косой';
						if($product->tail == 2) $tail .= ' прямой';
						echo $tail;
					?>
				</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->staff_with_professional_sheet == 2) { ?>
			<tr>
				<td style="text-align: left; ">
					<?php 
						$prof = 'Заполнение профлистом';
						if($product->profiled_sheet_S8045mm == 1) {
							$prof .= ' Профлист С8 0,45мм цвет с одной стороны.';
							if($product->compl_proflist == 1) $prof .= ' Заполнение с одной стороны';
							if($product->compl_proflist == 2) $prof .= ' Заполнение с двух сторон';	
						} elseif($product->profiled_sheet_S8045mm == 2) {
							$prof .= ' Профлист С8 0,45мм цвет с двух сторон';	
						}
						
						echo $prof;
					?>
				</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->filling_out == 1) { ?>
			<tr>
				<td style="text-align: left; ">Заполнение 3Д сетка</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->filling_out == 2) { ?>
			<tr>
				<td style="text-align: left; ">Заполнение "Решётка"</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['resRack'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Рейка зубчатая</td>
				<td><?php echo $product->toothed_rack_count;?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['summ'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Калитка встроенная</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['zamSum'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Замок врезной Stublina</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['bZ'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Закладная на ворота 4м с арматурой для монтажа на бетон</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['shV'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Швеллер П12</td>
				<td><?php echo $product->channel_P12?></td><td>м</td>
			</tr>
		<?php } ?>
		<?php if($prices['summFreeGateF'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Калитка отдельностоящая</td>
				<td>1</td><td>шт</td>
			</tr>
			<tr>
				<td style="text-align: left; ">
					<?php if($product->type_freestanding_gate == 1) {
						$rama = 'Рама калитки из профиля ';
						if($product->frame_profile == 1) $rama .= '60x30';
						if($product->frame_profile == 2) $rama .= '60x60';
						if($product->frame_profile == 3) $rama .= '80х80/80х40';
						echo $rama;
					}?>
					<?php if($product->type_freestanding_gate == 2) {
						$rama = 'Со столбами ';
						if($product->columns_r == 1) $rama .= '60x30';
						if($product->columns_r == 2) $rama .= '60x60';
						if($product->columns_r == 3) $rama .= '80х80';
						echo $rama;
					}?>
					<?php if($product->type_freestanding_gate == 3) {
						$rama = 'Без столбов и рамы ';
						echo $rama;
					}?>
				</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['zamSumV'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Замок врезной Stublina</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['sumPF'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">
					<?php 
						$pf = '';
						if($product->profiled_sheet_S8045mm == 1) $pf .= 'Профлист С8 0,45мм цвет с одной стороны';
						if($product->profiled_sheet_S8045mm == 2) $pf .= 'Профлист С8 0,45мм цвет с двух сторон';
						echo $pf;
					?>
				</td>
				<td><?php echo $prices['d']*$prices['hList']; ?></td><td>пог. м</td>
			</tr>
		<?php } ?>
		<?php if($product->type_freestanding_gate == 1) { ?>
			<tr>
				<td style="text-align: left; ">E</td>
				<td><?php echo $product->frame_width;?></td><td>мм</td>
			</tr>
			<tr>
				<td style="text-align: left; ">K</td>
				<td><?php echo $product->frame_height;?></td><td>мм</td>
			</tr>
		<?php } ?>
		<?php if($product->type_freestanding_gate == 2) { ?>
			<tr>
				<td style="text-align: left; ">E</td>
				<td><?php echo $product->gate_columns;?></td><td>мм</td>
			</tr>
			<tr>
				<td style="text-align: left; ">K</td>
				<td><?php echo $product->gate_height;?></td><td>мм</td>
			</tr>
		<?php } ?>
		<?php if($product->type_freestanding_gate == 3) { ?>
			<tr>
				<td style="text-align: left; ">E</td>
				<td><?php echo $product->width_without_cf;?></td><td>мм</td>
			</tr>
			<tr>
				<td style="text-align: left; ">K</td>
				<td><?php echo $product->height_without_cf;?></td><td>мм</td>
			</tr>
		<?php } ?>
		<?php if($prices['sumPF'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Длина профлиста</td>
				<td><?php echo $prices['hList']*1000;?></td><td>мм</td>
			</tr>
		<?php } ?>
		<?php if($prices['packSheet'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Упаковка профлиста</td>
				<td><?php echo $prices['packSheet']/250;?></td><td>пог. м</td>
			</tr>
		<?php } ?>
		<?php if($prices['samrez'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Кровельные саморезы</td>
				<td><?php echo $prices['samrez']/2.50;?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->pile_57x2000mm > 0) { ?>
			<tr>
				<td style="text-align: left; ">D57*3,5мм 2000мм</td>
				<td><?php echo $product->pile_57x2000mm; ?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->pile_76x2000mm > 0) { ?>
			<tr>
				<td style="text-align: left; ">D76*3,5мм 2000мм</td>
				<td><?php echo $product->pile_76x2000mm; ?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->pile_89x2000mm > 0) { ?>
			<tr>
				<td style="text-align: left; ">D89*3,5мм 2000мм</td>
				<td><?php echo $product->pile_89x2000mm; ?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->pile_108x2000mm > 0) { ?>
			<tr>
				<td style="text-align: left; ">D108*3,5мм 2000мм</td>
				<td><?php echo $product->pile_108x2000mm; ?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->heading_57 > 0) { ?>
			<tr>
				<td style="text-align: left; ">Оголовок D57</td>
				<td><?php echo $product->heading_57; ?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->heading_76 > 0) { ?>
			<tr>
				<td style="text-align: left; ">Оголовок D76</td>
				<td><?php echo $product->heading_76; ?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->heading_89 > 0) { ?>
			<tr>
				<td style="text-align: left; ">Оголовок D89</td>
				<td><?php echo $product->heading_89; ?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->heading_108 > 0) { ?>
			<tr>
				<td style="text-align: left; ">Оголовок D108</td>
				<td><?php echo $product->heading_108; ?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['automatSummAl'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">
					<?php $autoAl = 'Комплект автоматики Алютех '; 
						if($product->drive_unit == 1) $autoAl .= 'RTO500KIT';
						if($product->drive_unit == 2) $autoAl .= 'RTO1000KIT';
						if($product->drive_unit == 3) $autoAl .= 'RTO2000KIT';
						$autoAl .= ' (привод, 4х канальные пульты 2шт, монтажная пластина)';
						echo $autoAl;
					?>
				</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['lampAl'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Сигнальная лампа Алютех SL-U</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['photoAl'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Фотоэлементы Алютех LM-L</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['pultAl'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Брелок передатчик AT-4N</td>
				<td><?php echo $prices['pultAl']/1045; ?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['automatSummCombo'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">
					<?php $autoCombo = 'Комплект автоматики CAME '; 
						if($product->combo_kit == 1) $autoCombo .= 'Combo BX608';
						if($product->combo_kit == 2) $autoCombo .= 'Combo BX708';
						$autoCombo .= ' (привод, 4х канальные пульты 2шт, монтажная пластина)';
						echo $autoCombo;
					?>
				</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['lampCombo'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">CAME 001KLED Сигнальная лампа (светодиодная) 230В</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['antennaCombo'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Антенна САМЕ</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['pultCombo'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Брелок-передатчик 4-х канальный TOP44RGR</td>
				<td><?php echo $prices['pultCombo']/1320; ?></td><td>шт</td>
			</tr>
		<?php } ?>
	</table>
				</td><td><img src="<?php echo $prices['GatePath'];?>" style="width: 300px; height: auto;"></td>
		</tr>
	</table>
    
    <pagebreak>
		<h3 style="font-family: Calibri; margin-top: 20px; text-align: center; font-size: 12px;"><?="Спецификация к заказу №{$model->id} от ".Yii::$app->formatter->asDatetime($model->created_at, 'php:d.m.Y')?></h3>
	<table style="width: 100%;  border: 1px solid #000; padding-top: -9px;" class="pdf-kp">
		
       	<tr style="border: 1px solid #000;"><td style="border: none;"></td></tr>
		
		<tr>
            <td style="width: 70%; text-align: center; font-size: 10px;">Наименование</td>
			<td style="width: 15%; text-align: center; font-size: 10px;">Кол-во</td>
			<td style="width: 15%; text-align: center; font-size: 10px;">Ед. изм.</td>
		</tr>
		<?php if($prices['zapRes'] > 0) { ?>
			<tr>
				<td style="text-align: left; "><?php $roll = 'Откатные ворота '.$product->width.'x'.$product->height;
					if($product->beam == 1) $roll .= ' Эко SGN 01';
					if($product->beam == 2) $roll .= ' Евро SGN 02';
					if($product->filling_out == 3) $roll .= ' под зашивку профлистом';
					if($product->filling_out == 1) $roll .= ' ЗД сетка';
					if($product->filling_out == 2) $roll .= ' ЗД "решетка"';
					echo $roll;
					?>
				</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['zapUstr'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Запорное устройство</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['resRack'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Рейка зубчатая</td>
				<td><?php echo $product->toothed_rack_count;?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['summ'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Калитка встроенная</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['zamSum'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Замок врезной Stublina</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['bZ'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Закладная на ворота 4м с арматурой для монтажа на бетон</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['shV'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Швеллер П12</td>
				<td><?php echo $product->channel_P12; ?></td><td>м</td>
			</tr>
		<?php } ?>
		<?php if($prices['summFreeGateF'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Калитка отдельностоящая</td>
				<td>1</td><td>шт</td>
			</tr>
			<tr>
				<td style="text-align: left; ">
					<?php if($product->type_freestanding_gate == 1) {
						$rama = 'Рама калитки из профиля ';
						if($product->frame_profile == 1) $rama .= '60x30';
						if($product->frame_profile == 2) $rama .= '60x60';
						if($product->frame_profile == 3) $rama .= '80х80/80х40';
						echo $rama;
					}?>
					<?php if($product->type_freestanding_gate == 2) {
						$rama = 'Со столбами ';
						if($product->columns_r == 1) $rama .= '60x30';
						if($product->columns_r == 2) $rama .= '60x60';
						if($product->columns_r == 3) $rama .= '80х80';
						echo $rama;
					}?>
					<?php if($product->type_freestanding_gate == 3) {
						$rama = 'Без столбов и рамы ';
						echo $rama;
					}?>
				</td>
				
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['zamSumV'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Замок врезной Stublina</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['sumPF'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">
					<?php 
						$pf = '';
						if($product->profiled_sheet_S8045mm == 1) $pf .= 'Профлист С8 0,45мм цвет с одной стороны';
						if($product->profiled_sheet_S8045mm == 2) $pf .= 'Профлист С8 0,45мм цвет с двух сторон';
						echo $pf;
					?>
				</td>
				<td><?php echo $prices['d']*$prices['hList']; ?></td><td>пог. м</td>
			</tr>
		<?php } ?>
		<?php if($prices['sumPF'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Длина профлиста</td>
				<td><?php echo $prices['hList']*1000;?></td><td>мм</td>
			</tr>
		<?php } ?>
		<?php if($prices['packSheet'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Упаковка профлиста</td>
				<td><?php echo $prices['packSheet']/250;?></td><td>пог. м</td>
			</tr>
		<?php } ?>
		<?php if($prices['samrez'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Кровельные саморезы</td>
				<td><?php echo $prices['samrez']/2.50;?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->pile_57x2000mm > 0) { ?>
			<tr>
				<td style="text-align: left; ">D57*3,5мм 2000мм</td>
				<td><?php echo $product->pile_57x2000mm; ?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->pile_76x2000mm > 0) { ?>
			<tr>
				<td style="text-align: left; ">D76*3,5мм 2000мм</td>
				<td><?php echo $product->pile_76x2000mm; ?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->pile_89x2000mm > 0) { ?>
			<tr>
				<td style="text-align: left; ">D89*3,5мм 2000мм</td>
				<td><?php echo $product->pile_89x2000mm; ?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->pile_108x2000mm > 0) { ?>
			<tr>
				<td style="text-align: left; ">D108*3,5мм 2000мм</td>
				<td><?php echo $product->pile_108x2000mm; ?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->heading_57 > 0) { ?>
			<tr>
				<td style="text-align: left; ">Оголовок D57</td>
				<td><?php echo $product->heading_57; ?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->heading_76 > 0) { ?>
			<tr>
				<td style="text-align: left; ">Оголовок D76</td>
				<td><?php echo $product->heading_76; ?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->heading_89 > 0) { ?>
			<tr>
				<td style="text-align: left; ">Оголовок D89</td>
				<td><?php echo $product->heading_89; ?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($product->heading_108 > 0) { ?>
			<tr>
				<td style="text-align: left; ">Оголовок D108</td>
				<td><?php echo $product->heading_108; ?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['automatSummAl'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">
					<?php $autoAl = 'Комплект автоматики Алютех '; 
						if($product->drive_unit == 1) $autoAl .= 'RTO500KIT';
						if($product->drive_unit == 2) $autoAl .= 'RTO1000KIT';
						if($product->drive_unit == 3) $autoAl .= 'RTO2000KIT';
						$autoAl .= ' (привод, 4х канальные пульты 2шт, монтажная пластина)';
						echo $autoAl;
					?>
				</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['lampAl'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Сигнальная лампа Алютех SL-U</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['photoAl'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Фотоэлементы Алютех LM-L</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['pultAl'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Брелок передатчик AT-4N</td>
				<td><?php echo $prices['pultAl']/1045; ?></td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['automatSummCombo'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">
					<?php $autoCombo = 'Комплект автоматики CAME '; 
						if($product->combo_kit == 1) $autoCombo .= 'Combo BX608';
						if($product->combo_kit == 2) $autoCombo .= 'Combo BX708';
						$autoCombo .= ' (привод, 4х канальные пульты 2шт, монтажная пластина)';
						echo $autoCombo;
					?>
				</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['lampCombo'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">CAME 001KLED Сигнальная лампа (светодиодная) 230В</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['antennaCombo'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Антенна САМЕ</td>
				<td>1</td><td>шт</td>
			</tr>
		<?php } ?>
		<?php if($prices['pultCombo'] > 0) { ?>
			<tr>
				<td style="text-align: left; ">Брелок-передатчик 4-х канальный TOP44RGR</td>
				<td><?php echo $prices['pultCombo']/1320; ?></td><td>шт</td>
			</tr>
		<?php } ?>
	
    </table>
		<br>
	<pagebreak>
		<?php if($counter < count($model->products)-1): ?>
        <?php endif; ?>
        <?php $counter++; ?>
        <?php endforeach; ?>



		

	
	
    
		