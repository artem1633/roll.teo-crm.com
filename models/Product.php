<?php

namespace app\models;

use app\validators\ParamTypeValidator;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name Название
 * @property int $order_id Заказ
 * @property int $user_id Менеджер
 * @property double $price Цена
 * @property double $sum Сумма
 * @property integer $count Кол-во
 * @property int $category Категория
 * @property int $direction Направление
 * @property double $height Высота
 * @property double $width Ширина
 * @property double $additional_width Дополнительная ширина
 * @property double $canvas_width Ширина полотна
 * @property string $ral RAL (Цвет)
 * @property int $metal Метал
 * @property int $castle Замок
 * @property int $handle Ручка
 * @property int $glass_window Стеклопакет
 * @property int $chipper Отбойник
 * @property int $closer Доводчик
 * @property int $transom Фрамуга
 * @property string $delivery Доставка
 * @property string $install Монтаж/Демонтаж
 * @property string $additional_information Дополнительная информация
 * @property string $comment Примечание
 * @property string $created_at
 *
 * @property string $imgPath Изображение
 *
 * @property Param $castleParam
 * @property Param $categoryParam
 * @property Param $chipperParam
 * @property Param $closerParam
 * @property Param $directionParam
 * @property Param $glassWindowParam
 * @property Param $handleParam
 * @property Param $metalParam
 * @property Order $order
 * @property Param $transomParam
 * @property User $user
 * @property ProductComment[] $productComments
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $comments;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => null,
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['order_id', 'user_id', 
			  'rollback_side', 'staff_with_professional_sheet', 'profiled_sheet_S8045mm', 'filling_out', 
			  'beam', 'builtin_gate', 'gate_location_w', 'gate_opening_w', 'wicket_lock', 'profiled_sheet_S8045mm_ko',
			  'filling_out_freestanding_gate', 'type_freestanding_gate', 'frame_width', 'frame_height', 'frame_profile', 
			  'gate_columns', 'columns_r', 'height60x60', 'height80x80', 'tail_distance', 'tail_pr',
			  'gate_opening', 'hinges', 'opening_side', 'lock_gate', 'toothed_rack', 'tail', 'locking_device', 'dyeing', 'compl_proflist',
			  'concrete_mortgage', 'pile_57x2000mm', 'heading_57', 'pile_76x2000mm', 'heading_76', 'pile_89x2000mm', 'heading_89', 'compl_proflist_ko',
			  'pile_108x2000mm', 'heading_108', 'automation', 'drive_unit', 'photocells', 'lamp', 'combo_kit', 'antenna', 'color'], 'integer'],
            [['height', 'width', 'price', 'gate_height', 'wicket_width', 'count', 'height_without_cf', 'width_without_cf', 'dop_pult', 'channel_P12', 'toothed_rack_count'], 'number'],
            [['created_at', 'comments'], 'safe'],
            [['name', 'delivery', 'install'], 'string', 'max' => 255],
            [['additional_information', 'comment', 'arr_prices'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'order_id' => 'Заказ',
            'count' => 'Кол-во',
            'user_id' => 'Менеджер',
            'price' => 'Сумма',
            'category' => 'Категория',
            'color' => 'Цвет',
            'height' => 'Высота',
            'width' => 'Ширина',
			'compl_proflist' => 'Заполнение профлистом',
			'compl_proflist_ko' => 'Заполнение профлистом',
			'rollback_side' => 'Сторона отката(вид изнутри)',
			'staff_with_professional_sheet' => 'Укомплектовать профлистом',
			'profiled_sheet_S8045mm' => 'Профлист С8 0,45мм цвет',
			'filling_out' => 'Заполнение',
			'staff_with_professional_sheet_ko' => 'Укомплектовать профлистом',
			'profiled_sheet_S8045mm_ko' => 'Профлист С8 0,45мм цвет',
			'beam' => 'Балка',
			'builtin_gate' => 'Калитка встроеная',
			'wicket_width' => 'Ширина калитки встроенной, мм',
			'gate_location_w' => 'Расположение калитки встроенной',
			'tail_distance' => 'Расстояние от хвоста, мм',
			'gate_opening_w' => 'Открывание калитки встроенной',
			'wicket_lock' => 'Замок калитки встроенной',
			'filling_out_freestanding_gate' => 'Заполнение(калитка отдельностоящая)',
			'type_freestanding_gate' => 'Тип(калитка отдельностоящая)',
			'gate_opening' => 'Открывание калитки',
			'lock_gate' => 'Замок для калитки',
			'channel_P12' => 'Швеллер П12',
			'concrete_mortgage' => 'Закладная для бетонирования',
			'pile_57x2000mm' => 'Свая 57х2000мм',
			'heading_57' => 'Оголовок 57',
			'pile_76x2000mm' => 'Свая 76х2000мм',
			'heading_76' => 'Оголовок 76',
			'pile_89x2000mm' => 'Свая 89х2000мм',
			'heading_89' => 'Оголовок 89',
			'pile_108x2000mm' => 'Свая 108х2000мм',
			'heading_108' => 'Оголовок 108',
			'automation' => 'Автоматика',
			'drive_unit' => 'Привод',
			'photocells' => 'Фотоэлементы',
			'lamp' => 'Лампа',
			'combo_kit' => 'Комплект Combo',
			'antenna' => 'Антенна',
			'frame_width' => 'Ширина рамы',
			'frame_height' => 'Высота от верха рамы до низа калитки',
			'frame_profile' => 'Профиль рамы',
			'gate_columns' => 'Ширина по столбам',
			'gate_height' => 'Высота калитки',
			'columns_r' => 'Столбы',
			'height60x60' => 'Высота(60х60)',
			'height80x80' => 'Высота(80х80)',
			'height_without_cf' => 'Высота(без столбов и рамы), мм',
			'width_without_cf' => 'Ширина(без столбов и рамы), мм',
			'hinges' => 'Петли',
			'opening_side' => 'Сторона открывания',
			'toothed_rack' => 'Рейка зубчатая',
			'toothed_rack_count' => 'Количество реек зубчатых',
			'tail' => 'Хвост',
			'locking_device' => 'Запорное устройство',
			'dyeing' => 'Покраска',
            'delivery' => 'Доставка',
            'install' => 'Монтаж/Демонтаж',
            'additional_information' => 'Дополнительная информация',
            'created_at' => 'Дата и время создания',
            'comments' => 'Примечание',
			'tail_pr' => 'Хвост прямой',
			'dop_pult' => 'Дополнительные пульты',
        ];
    }


    /**
     * @inheritdoc
     */
    public function getSum()
    {
        return $this->price * $this->count;
    }

    /**
     * @inheritdoc
     */
   /* public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        /*if($this->comments != null){
            ProductComment::deleteAll(['product_id' => $this->id]);
            foreach ($this->comments as $comment){
                (new ProductComment(['product_id' => $this->id, 'comment_id' => $comment]))->save(false);
            }
        }

        /*if($this->categoryParam != null && $this->categoryParam->img_url != ''){
            $img = imagecreatefrompng($this->categoryParam->img_url);
        }
        if($this->handleParam != null && $this->handleParam->img_url != ''){
            $img2 = imagecreatefrompng($this->handleParam->img_url);
            imagecopy($img, $img2, 0, 0, 0, 0, 1100, 1100);
        }
        if($this->glassWindowParam != null && $this->glassWindowParam->img_url != ''){
            $img3 = imagecreatefrompng($this->glassWindowParam->img_url);
            imagecopy($img, $img3, 0, 0, 0, 0, 1000, 1000);
        }
        if($this->transomParam != null && $this->transomParam->img_url != ''){
            $img4 = imagecreatefrompng($this->transomParam->img_url);
            imagecopy($img, $img4, 0, 0, 0, 0, 1000, 1000);
        }
        if($this->chipperParam != null && $this->chipperParam->img_url != ''){
            $img5 = imagecreatefrompng($this->chipperParam->img_url);
            imagecopy($img, $img5, 0, 0, 0, 0, 1500, 1500);
        }

        if(is_dir('product') == false){
            mkdir('product');
        }

        imagepng($img, 'product/image'.$this->id.'.png');
    }*/

    /**
     * @inheritdoc
     */
    public function getImgPath()
    {
       return "product/image{$this->id}.png";
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
