<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company_address".
 *
 * @property int $id
 * @property int $company_id Компания
 * @property string $address Адрес
 *
 * @property AddressSpecification[] $addressSpecifications
 * @property Company $company
 */
class CompanyAddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address', 'company_id'], 'required'],
            [['company_id'], 'integer'],
            [['address'], 'string', 'max' => 500],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'address' => 'Адрес',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressSpecifications()
    {
        return $this->hasMany(AddressSpecification::className(), ['address_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
