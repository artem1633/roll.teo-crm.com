<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Product;

/**
 * ProductSearch represents the model behind the search form about `app\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'user_id', 'category', 
			  'rollback_side', 'staff_with_professional_sheet', 'profiled_sheet_S8045mm', 'filling_out', 
			  'beam', 'builtin_gate', 'gate_location_w', 'gate_opening_w', 'wicket_lock', 'profiled_sheet_S8045mm_ko',
			  'filling_out_freestanding_gate', 'type_freestanding_gate', 'frame_width', 'frame_height', 'frame_profile', 
			  'gate_columns', 'columns_r', 'height60x60', 'height80x80', 'tail_distance', 'tail_pr', 'compl_proflist_ko',
			  'gate_opening', 'hinges', 'opening_side', 'lock_gate', 'toothed_rack', 'tail', 'locking_device', 'dyeing',
			  'concrete_mortgage', 'pile_57x2000mm', 'heading_57', 'pile_76x2000mm', 'heading_76', 'pile_89x2000mm', 'heading_89',
			  'pile_108x2000mm', 'heading_108', 'automation', 'drive_unit', 'photocells', 'lamp', 'combo_kit', 'antenna', 'color'], 'integer'],
            ['count', 'default', 'value' => 0],
            [['height', 'width', 'price', 'gate_height', 'wicket_width', 'count', 'height_without_cf', 'width_without_cf', 'dop_pult', 'channel_P12', 'toothed_rack_count'], 'number'],
            [['created_at', 'comments'], 'safe'],
            [['name', 'delivery', 'install'], 'string', 'max' => 255],
            [['additional_information', 'comment'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'user_id' => $this->user_id,
            'price' => $this->price,
            'category' => $this->category,
            'height' => $this->height,
            'width' => $this->width,
			'color' => $this->color,
			'rollback_side' => $this->rollback_side,
			'staff_with_professional_sheet' => $this->staff_with_professional_sheet,
			'profiled_sheet_S8045mm' => $this->profiled_sheet_S8045mm,
			'filling_out' => $this->filling_out,
			'staff_with_professional_sheet_ko' => $this->staff_with_professional_sheet_ko,
			'profiled_sheet_S8045mm_ko' => $this->profiled_sheet_S8045mm_ko,
			'beam' => $this->beam,
			'builtin_gate' => $this->builtin_gate,
			'wicket_width' => $this->wicket_width,
			'gate_location_w' => $this->gate_location_w,
			'tail_distance' => $this->tail_distance,
			'toothed_rack_count' => $this->toothed_rack_count,
			'gate_opening_w' => $this->gate_opening_w,
			'wicket_lock' => $this->wicket_lock,
			'tail_pr' => $this->tail_pr,
			'dop_pult' => $this->dop_pult,
			'compl_proflist' => $this->compl_proflist,
			'compl_proflist_ko' => $this->compl_proflist_ko,
			'filling_out_freestanding_gate' => $this->filling_out_freestanding_gate,
			'type_freestanding_gate' => $this->type_freestanding_gate,
			'gate_opening' => $this->gate_opening,
			'lock_gate' => $this->lock_gate,
			'channel_P12' => $this->channel_P12,
			'concrete_mortgage' => $this->concrete_mortgage,
			'pile_57x2000mm' => $this->pile_57x2000mm,
			'heading_57' => $this->heading_57,
			'pile_76x2000mm' => $this->pile_76x2000mm,
			'heading_76' => $this->heading_76,
			'pile_89x2000mm' => $this->pile_89x2000mm,
			'heading_89' => $this->heading_89,
			'pile_108x2000mm' => $this->pile_108x2000mm,
			'heading_108' => $this->heading_108,
			'automation' => $this->automation,
			'drive_unit' => $this->drive_unit,
			'photocells' => $this->photocells,
			'lamp' => $this->lamp,
			'combo_kit' => $this->combo_kit,
			'antenna' => $this->antenna,
			'frame_width' => $this->frame_width,
			'frame_height' => $this->frame_height,
			'frame_profile' => $this->frame_profile,
			'gate_columns' => $this->gate_columns,
			'gate_height' => $this->gate_height,
			'columns_r' => $this->columns_r,
			'height60x60' => $this->height60x60,
			'height80x80' => $this->height80x80,
			'height_without_cf' => $this->height_without_cf,
			'width_without_cf' => $this->width_without_cf,
			'hinges' => $this->hinges,
			'opening_side' => $this->opening_side,
			'toothed_rack' => $this->toothed_rack,
			'tail' => $this->tail,
			'locking_device' => $this->locking_device,
			'dyeing' => $this->dyeing,
            'delivery' => $this->delivery,
            'install' => $this->install,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
