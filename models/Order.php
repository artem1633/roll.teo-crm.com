<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use app\models\Product;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property int $user_id Менеджер
 * @property string $name Наименование
 * @property string $address Адрес
 * @property string $client Клиент
 * @property integer $status_id Статус
 * @property string $date Дата
 * @property string $created_at
 *
 * @property User $user
 * @property Product[] $products
 * @property OrderStatus $status
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	
	/*public $rollPrice;
	public $zapUstrPrice;
	public $reikaPrice;
	public $gateInPrice;
	public $zamokPrice;
	public $zakladPrice;
	public $shvellerPrice;
	public $gateOutPrice;
	public $ramaPrice;
	public $zamokGatePrice;
	public $profSheetPrice;
	public $packProfSheetPrice;
	public $samrezPrice;
	public $svai57Price;
	public $svai76Price;
	public $svai89Price;
	public $svai108Price;
	public $ogolovok57Price;
	public $ogolovok76Price;
	public $ogolovok89Price;
	public $ogolovok108Price;
	public $automatPrice;
	public $automatLampaPrice;
	public $automatDopPultPrice;
	public $automatAntenaPrice;
	public $automatPhotoElPrice;*/
	//public $arr_prices;
	
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => null,
                'value' => Yii::$app->user->getId()
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'address'], 'required'],
            [['user_id', 'status_id'], 'integer'],
            [['date', 'created_at'], 'safe'],
            [['name', 'address', 'client'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Клиент',
            'name' => 'Наименование',
            'address' => 'Адрес',
            'client' => 'Коммент',
            'status_id' => 'Статус',
            'date' => 'Дата',
            'created_at' => 'Дата и время создания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['order_id' => 'id']);
    }
}
