<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Image extends Model
{
    public $image;
    public $url;

    /**
     * @return array
     */
    public function rules()
    {
        $array_rule = [
            [['image'], 'file', 'extensions' => 'png, jpg, jpeg'],
            ['url', 'string'],
        ];

        return $array_rule;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'image' => 'Изображение чертежа',
            'url' => 'тип',
        ];
    }

    /**
     * @return bool
     */
    public function upload($url)
    {
        if ($this->validate()) {
            $this->image->saveAs("img/{$url}/{$this->image->baseName}.{$this->image->extension}");
        } else {
            return false;
        }
    }
}

